package proto

import (
	"stream-scheduler/types"
	"time"

	"github.com/asvedr/gotgbot/v2"
)

type ITimeSerDe interface {
	Parse(src string) (time.Time, error)
	Show(tm time.Time) (string, error)
}

type ITgUseCase interface {
	Command() string
	Execute(chat int64, sender types.User, text string) (string, error)
}

type IBot interface {
	Reconnect(opts *gotgbot.BotOpts) error
	SendMessage(
		chatId int64,
		text string,
		opts *gotgbot.SendMessageOpts,
	) (*gotgbot.Message, error)
}
