package proto

import (
	"stream-scheduler/types"
	"time"
)

type ISender interface {
	Send(chat int64, text string) error
}

type IProcess interface {
	Name() string
	Run()
}

type INow interface {
	Now() time.Time
}

type IPeriodic interface {
	Execute() error
}

type IStreamerPicker interface {
	GetStreamer(string) (*types.Streamer, error)
}
