package proto

import (
	t "stream-scheduler/types"
	"time"
)

type ISettingsRepo interface {
	Get() (t.Settings, error)
	Set(settings t.Settings) error
}

type IStreamersRepo interface {
	Create(streamer t.NewStreamer) (int, error)
	Remove(streamer_id int) error
	GetById(id int) (*t.Streamer, error)
	FindByTg(tg_id *int64, tg_login *string) (*t.Streamer, error)
	FindByName(name string) (*t.Streamer, error)
	GetAll() ([]*t.Streamer, error)
	GetByTag(text string) ([]*t.Streamer, error)
	UpdName(streamer_id int, name string, tags []string) error
	UpdTg(streamer_id int, tg int64) error
	AddLinks(streamer_id int, links []string) error
	RemoveLinks(streamer_id int, links []string) error
}

type IEventsRepo interface {
	Create(event t.NewEvent) (int, error)
	UpdateInfo(t.UpdEventInfo) error
	UpdateTime(event_id int, when time.Time) error
	AddNotifTimes(event_id int, seconds_before []int) error
	GetNotifTimes(event_id int) (map[time.Time]bool, error)
	Remove(event_id int) error
	GetById(event_id int) (*t.Event, error)
	GetChatEvents(chat_id int64, filter_expired bool) ([]*t.Event, error)
	GetEventsToNotify() ([]*t.EventWithSeconds, error)
	SetSendNotifications(event_id int, seconds_before []int) error
	RemoveOld() error
}

type IUsersRepo interface {
	CreateOrUpdate(t.User) (*t.User, error)
	GetById(id int64) (*t.User, error)
	GetByLogin(login string) (*t.User, error)
}
