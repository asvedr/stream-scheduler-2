package app

import (
	"stream-scheduler/components/sender"
	"stream-scheduler/components/streamer_picker"
	"stream-scheduler/components/time_serde"
	"stream-scheduler/proto"

	"github.com/asvedr/gotgbot/v2"
	"gitlab.com/asvedr/cldi/di"
)

var TimeSerDe = di.Singleton[proto.ITimeSerDe]{
	PureFunc: func() proto.ITimeSerDe {
		return time_serde.New(Now.Get(), SettingsRepo.Get())
	},
}

var Bot = di.Factory[*gotgbot.Bot]{
	PureFunc: func() *gotgbot.Bot {
		token := Config.Get().Token
		return gotgbot.NewBotNoConnect(token, nil)
	},
}

var Sender = di.Factory[proto.ISender]{
	PureFunc: func() proto.ISender {
		return sender.New(Config.Get(), Bot.Get())
	},
}

var StreamerPicker = di.Singleton[proto.IStreamerPicker]{
	PureFunc: func() proto.IStreamerPicker {
		return streamer_picker.New(
			StreamersRepo.Get(),
			UsersRepo.Get(),
		)
	},
}
