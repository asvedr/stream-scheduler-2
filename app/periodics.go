package app

import (
	"stream-scheduler/periodics/clean_events"
	"stream-scheduler/periodics/send_notifications"
	"stream-scheduler/proto"

	"gitlab.com/asvedr/cldi/di"
)

var CleanPeriodic = di.Factory[proto.IPeriodic]{
	PureFunc: func() proto.IPeriodic {
		return clean_events.New(EventsRepo.Get())
	},
}

var SendNotifsPeriodic = di.Factory[proto.IPeriodic]{
	PureFunc: func() proto.IPeriodic {
		return send_notifications.New(
			EventsRepo.Get(),
			SettingsRepo.Get(),
			StreamersRepo.Get(),
			TimeSerDe.Get(),
			Sender.Get(),
			Now.Get(),
		)
	},
}
