package app

import (
	"stream-scheduler/proto"
	"stream-scheduler/tg_use_cases/add_event"
	"stream-scheduler/tg_use_cases/add_streamer"
	"stream-scheduler/tg_use_cases/add_streamer_link"
	"stream-scheduler/tg_use_cases/check_time"
	"stream-scheduler/tg_use_cases/get_event"
	"stream-scheduler/tg_use_cases/get_streamer"
	"stream-scheduler/tg_use_cases/help"
	"stream-scheduler/tg_use_cases/rename_streamer"
	"stream-scheduler/tg_use_cases/rm_event"
	"stream-scheduler/tg_use_cases/rm_streamer"
	"stream-scheduler/tg_use_cases/rm_streamer_link"
	"stream-scheduler/tg_use_cases/rm_tz"
	"stream-scheduler/tg_use_cases/set_event_streamer"
	"stream-scheduler/tg_use_cases/set_lang"
	"stream-scheduler/tg_use_cases/set_streamer_tg"
	"stream-scheduler/tg_use_cases/set_tz"

	"gitlab.com/asvedr/cldi/di"
)

var SetTzUC = di.Singleton[proto.ITgUseCase]{
	PureFunc: func() proto.ITgUseCase {
		return set_tz.New(SettingsRepo.Get())
	},
}

var RmTzUC = di.Singleton[proto.ITgUseCase]{
	PureFunc: func() proto.ITgUseCase {
		return rm_tz.New(SettingsRepo.Get())
	},
}

var CheckTimeTzUC = di.Singleton[proto.ITgUseCase]{
	PureFunc: func() proto.ITgUseCase {
		return check_time.New(SettingsRepo.Get(), Now.Get())
	},
}

var HelpUC = di.Singleton[proto.ITgUseCase]{
	PureFunc: func() proto.ITgUseCase {
		return help.New(SettingsRepo.Get())
	},
}

var AddStreamerUC = di.Singleton[proto.ITgUseCase]{
	PureFunc: func() proto.ITgUseCase {
		return add_streamer.New(StreamersRepo.Get())
	},
}

var RenameStreamerUC = di.Singleton[proto.ITgUseCase]{
	PureFunc: func() proto.ITgUseCase {
		return rename_streamer.New(StreamersRepo.Get())
	},
}

var SetStreamerTgUC = di.Singleton[proto.ITgUseCase]{
	PureFunc: func() proto.ITgUseCase {
		return set_streamer_tg.New(
			StreamersRepo.Get(),
			UsersRepo.Get(),
		)
	},
}

var SetLangUC = di.Singleton[proto.ITgUseCase]{
	PureFunc: func() proto.ITgUseCase {
		return set_lang.New(SettingsRepo.Get())
	},
}

var AddStreamerLinkUC = di.Singleton[proto.ITgUseCase]{
	PureFunc: func() proto.ITgUseCase {
		return add_streamer_link.New(StreamersRepo.Get())
	},
}

var RmStreamerLinkUC = di.Singleton[proto.ITgUseCase]{
	PureFunc: func() proto.ITgUseCase {
		return rm_streamer_link.New(StreamersRepo.Get())
	},
}

var RmStreamer = di.Singleton[proto.ITgUseCase]{
	PureFunc: func() proto.ITgUseCase {
		return rm_streamer.New(StreamersRepo.Get())
	},
}

var AddEventUC = di.Singleton[proto.ITgUseCase]{
	PureFunc: func() proto.ITgUseCase {
		return add_event.New(
			EventsRepo.Get(),
			StreamersRepo.Get(),
			SettingsRepo.Get(),
			TimeSerDe.Get(),
			Now.Get(),
		)
	},
}

var SetEventStreamerUC = di.Singleton[proto.ITgUseCase]{
	PureFunc: func() proto.ITgUseCase {
		return set_event_streamer.New(
			EventsRepo.Get(),
			StreamersRepo.Get(),
			StreamerPicker.Get(),
		)
	},
}

var RemoveEventUC = di.Singleton[proto.ITgUseCase]{
	PureFunc: func() proto.ITgUseCase {
		return rm_event.New(EventsRepo.Get())
	},
}

var GetStreamerUC = di.Singleton[proto.ITgUseCase]{
	PureFunc: func() proto.ITgUseCase {
		return get_streamer.New(
			StreamersRepo.Get(),
			StreamerPicker.Get(),
		)
	},
}

var GetEventUC = di.Singleton[proto.ITgUseCase]{
	PureFunc: func() proto.ITgUseCase {
		return get_event.New(
			EventsRepo.Get(),
			StreamersRepo.Get(),
			TimeSerDe.Get(),
		)
	},
}
