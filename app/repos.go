package app

import (
	"stream-scheduler/components/sqlite/events_repo"
	"stream-scheduler/components/sqlite/settings_repo"
	"stream-scheduler/components/sqlite/streamers_repo"
	"stream-scheduler/components/sqlite/users_repo"
	"stream-scheduler/proto"

	"gitlab.com/asvedr/cldi/di"
	ss "gitlab.com/asvedr/safe_sqlite"
)

var db = di.Singleton[ss.Db]{
	ErrFunc: func() (ss.Db, error) {
		return ss.New(Config.Get().DbPath)
	},
}

var SettingsRepo = di.Singleton[proto.ISettingsRepo]{
	ErrFunc: func() (proto.ISettingsRepo, error) {
		return settings_repo.New(db.Get())
	},
}

var StreamersRepo = di.Singleton[proto.IStreamersRepo]{
	ErrFunc: func() (proto.IStreamersRepo, error) {
		return streamers_repo.New(db.Get())
	},
}

var UsersRepo = di.Singleton[proto.IUsersRepo]{
	ErrFunc: func() (proto.IUsersRepo, error) {
		return users_repo.New(
			Config.Get(),
			db.Get(),
		)
	},
}

var EventsRepo = di.Singleton[proto.IEventsRepo]{
	ErrFunc: func() (proto.IEventsRepo, error) {
		return events_repo.New(
			Config.Get(),
			db.Get(),
		)
	},
}
