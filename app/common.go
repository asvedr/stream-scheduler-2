package app

import (
	"stream-scheduler/components/now"
	"stream-scheduler/proto"
	"stream-scheduler/types"

	"gitlab.com/asvedr/cldi/cl"
	"gitlab.com/asvedr/cldi/di"
)

var Config = di.Singleton[*types.Config]{
	ErrFunc: cl.BuildConfig[types.Config],
}

var Now = di.Singleton[proto.INow]{
	PureFunc: now.New,
}
