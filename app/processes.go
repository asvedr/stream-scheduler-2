package app

import (
	"stream-scheduler/processes/periodic"
	"stream-scheduler/processes/ui"
	"stream-scheduler/proto"
	"time"

	"gitlab.com/asvedr/cldi/di"
)

var CleanerProc = di.Singleton[proto.IProcess]{
	PureFunc: func() proto.IProcess {
		name := "clean-events"
		task := CleanPeriodic.Get()
		return periodic.New(name, task, time.Second)
	},
}

var SenderProc = di.Singleton[proto.IProcess]{
	PureFunc: func() proto.IProcess {
		name := "send-notifs"
		task := SendNotifsPeriodic.Get()
		return periodic.New(name, task, time.Second)
	},
}

var UIProc = di.Singleton[proto.IProcess]{
	PureFunc: func() proto.IProcess {
		return ui.New(
			Config.Get(),
			Bot.Get(),
			UsersRepo.Get(),
			SetTzUC.Get(),
			RmTzUC.Get(),
			CheckTimeTzUC.Get(),
			SetLangUC.Get(),
			HelpUC.Get(),
			AddStreamerUC.Get(),
			RenameStreamerUC.Get(),
			SetStreamerTgUC.Get(),
			AddStreamerLinkUC.Get(),
			RmStreamerLinkUC.Get(),
			RmStreamer.Get(),
			AddEventUC.Get(),
			SetEventStreamerUC.Get(),
			RemoveEventUC.Get(),
			GetStreamerUC.Get(),
			GetEventUC.Get(),
		)
	},
}
