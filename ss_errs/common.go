package ss_errs

import (
	"errors"
	"fmt"
)

var ErrInvalidDateTime = errors.New("invalid datetime")

type ErrSQLError struct{ Cause error }
type ErrStreamerNotFound struct{}
type ErrEventNotFound struct{}
type ErrUserNotFound struct{}
type ErrInvalidTime struct {
	Cause error
}

func NewErrSQLError(err error) error {
	return ErrSQLError{Cause: err}
}

func (e ErrSQLError) Error() string {
	return fmt.Sprintf("ErrSQLError(%v)", e.Cause)
}

func (e ErrStreamerNotFound) Error() string {
	return "ErrStreamerNotFound"
}

func (e ErrEventNotFound) Error() string {
	return "ErrErrEventNotFound"
}

func (e ErrUserNotFound) Error() string {
	return "ErrUserNotFound"
}

func (e ErrInvalidTime) Error() string {
	return fmt.Sprintf("ErrInvalidTime(%v)", e.Cause)
}
