package time_serde_test

import (
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
)

func TestShowShort(t *testing.T) {
	setup := make_setup()

	res, err := setup.serde.Show(setup.now.time.Add(time.Minute * 60))
	assert.Nil(t, err)
	assert.Equal(t, "after 1h", res)

	res, err = setup.serde.Show(setup.now.time.Add(time.Minute * 20))
	assert.Nil(t, err)
	assert.Equal(t, "after 20m", res)

	res, err = setup.serde.Show(setup.now.time.Add(time.Minute * 70))
	assert.Nil(t, err)
	assert.Equal(t, "after 1h 10m", res)

	res, err = setup.serde.Show(setup.now.time.Add(-time.Minute))
	assert.Nil(t, err)
	assert.Equal(t, "now", res)
}

func TestShowToday(t *testing.T) {
	setup := make_setup()
	setup.now.time = time.Date(
		2020, 1, 1, 0, 0, 0, 0, time.UTC,
	)
	event := time.Date(
		2020, 1, 1, 15, 20, 0, 0, time.UTC,
	)

	res, err := setup.serde.Show(event)
	assert.Nil(t, err)
	assert.Equal(t, "today 18:20 msk\ntoday 17:20 cy", res)
}

func TestShowTomorrow(t *testing.T) {
	setup := make_setup()
	setup.now.time = time.Date(
		2020, 1, 1, 0, 0, 0, 0, time.UTC,
	)
	event := time.Date(
		2020, 1, 2, 15, 20, 0, 0, time.UTC,
	)

	res, err := setup.serde.Show(event)
	assert.Nil(t, err)
	assert.Equal(t, "tomorrow 18:20 msk\ntomorrow 17:20 cy", res)
}

func TestShowFull(t *testing.T) {
	setup := make_setup()
	setup.now.time = time.Date(
		2020, 1, 1, 0, 0, 0, 0, time.UTC,
	)
	event := time.Date(
		2020, 1, 3, 15, 20, 0, 0, time.UTC,
	)

	res, err := setup.serde.Show(event)
	assert.Nil(t, err)
	assert.Equal(t, "03.01.2020 18:20 msk\n03.01.2020 17:20 cy", res)
}
