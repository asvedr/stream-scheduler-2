package time_serde_test

import (
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
)

func TestAfterParse(t *testing.T) {
	setup := make_setup()
	tm, err := setup.serde.Parse("after 5 min")
	assert.Nil(t, err)
	assert.Equal(
		t,
		setup.now.time.Add(time.Minute*5),
		tm,
	)

	tm, err = setup.serde.Parse("через 2 минуты")
	assert.Nil(t, err)
	assert.Equal(
		t,
		setup.now.time.Add(time.Minute*2),
		tm,
	)
}

func TestOnlyTimeParse(t *testing.T) {
	setup := make_setup()
	tm, err := setup.serde.Parse("14:40")
	assert.Nil(t, err)
	// default tz is msk+3
	assert.Equal(
		t,
		time.Date(2020, 1, 1, 11, 40, 0, 0, time.UTC),
		tm,
	)
}

func TestTodayParse(t *testing.T) {
	setup := make_setup()

	tm, err := setup.serde.Parse("today 14:40")
	assert.Nil(t, err)
	// default tz is msk+3
	assert.Equal(
		t,
		time.Date(2020, 1, 1, 11, 40, 0, 0, time.UTC),
		tm,
	)

	tm, err = setup.serde.Parse("14:40 сегодня")
	assert.Nil(t, err)
	// default tz is msk+3
	assert.Equal(
		t,
		time.Date(2020, 1, 1, 11, 40, 0, 0, time.UTC),
		tm,
	)
}

func TestTomorrowParse(t *testing.T) {
	setup := make_setup()

	tm, err := setup.serde.Parse("tomorrow 14:40")
	assert.Nil(t, err)
	// default tz is msk+3
	assert.Equal(
		t,
		time.Date(2020, 1, 2, 11, 40, 0, 0, time.UTC),
		tm,
	)

	setup.now.time = time.Date(2020, 1, 31, 0, 0, 0, 0, time.UTC)
	tm, err = setup.serde.Parse("14:40 завтра")
	assert.Nil(t, err)
	// default tz is msk+3
	assert.Equal(
		t,
		time.Date(2020, 2, 1, 11, 40, 0, 0, time.UTC),
		tm,
	)
}

func TestFullDateParse(t *testing.T) {
	setup := make_setup()

	tm, err := setup.serde.Parse("01.02.2023 14:40")
	assert.Nil(t, err)
	// default tz is msk+3
	assert.Equal(
		t,
		time.Date(2023, 2, 1, 11, 40, 0, 0, time.UTC),
		tm,
	)

	tm, err = setup.serde.Parse("14:40 1-2-2023")
	assert.Nil(t, err)
	// default tz is msk+3
	assert.Equal(
		t,
		time.Date(2023, 2, 1, 11, 40, 0, 0, time.UTC),
		tm,
	)
}

func TestTzParse(t *testing.T) {
	setup := make_setup()

	exp := time.Date(2020, 1, 1, 12, 40, 0, 0, time.UTC)
	tm, err := setup.serde.Parse("сегодня 14:40 cy")
	assert.Nil(t, err)
	assert.Equal(t, exp, tm)
	tm, err = setup.serde.Parse("сегодня cy 14:40")
	assert.Nil(t, err)
	assert.Equal(t, exp, tm)

	tm, err = setup.serde.Parse("cy 14:40 сегодня")
	assert.Nil(t, err)
	assert.Equal(t, exp, tm)

	tm, err = setup.serde.Parse("14:40 cy сегодня")
	assert.Nil(t, err)
	assert.Equal(t, exp, tm)

	tm, err = setup.serde.Parse("14:40 cy")
	assert.Nil(t, err)
	assert.Equal(t, exp, tm)

	tm, err = setup.serde.Parse("cy 14:40")
	assert.Nil(t, err)
	assert.Equal(t, exp, tm)
}
