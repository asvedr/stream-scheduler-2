package time_serde_test

import (
	"stream-scheduler/components/time_serde"
	"stream-scheduler/proto"
	"stream-scheduler/types"
	"time"
)

type mock_now struct {
	time time.Time
}

type mock_repo struct {
	data types.Settings
}

type setup struct {
	now      *mock_now
	settings *mock_repo
	serde    proto.ITimeSerDe
}

func (self *mock_now) Now() time.Time {
	return self.time
}

func (self *mock_repo) Get() (types.Settings, error) {
	return self.data, nil
}

func (self *mock_repo) Set(_ types.Settings) error {
	panic("")
}

func make_setup() setup {
	now := &mock_now{
		time: time.Date(2020, 1, 1, 10, 30, 0, 0, time.UTC),
	}
	stt := types.DefaultSettings()
	stt.Lang = types.LangEn
	repo := &mock_repo{
		data: stt,
	}
	return setup{
		now:      now,
		settings: repo,
		serde:    time_serde.New(now, repo),
	}
}
