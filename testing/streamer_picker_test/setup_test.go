package streamer_picker_test

import (
	"stream-scheduler/components/streamer_picker"
	"stream-scheduler/proto"
	"stream-scheduler/ss_errs"
	"stream-scheduler/types"
)

type mock_streamers struct {
	by_id    map[int]*types.Streamer
	by_tg_id map[int64]*types.Streamer
	by_name  map[string]*types.Streamer
	by_tag   map[string]*types.Streamer
}
type mock_users struct {
	by_login map[string]*types.User
	by_id    map[int64]*types.User
}

type setup struct {
	streamers *mock_streamers
	users     *mock_users
	picker    proto.IStreamerPicker
}

func (self *mock_streamers) GetById(id int) (*types.Streamer, error) {
	st := self.by_id[id]
	if st == nil {
		return nil, ss_errs.ErrStreamerNotFound{}
	}
	return st, nil
}
func (self *mock_streamers) FindByTg(tg_id *int64, tg_login *string) (*types.Streamer, error) {
	st := self.by_tg_id[*tg_id]
	if st == nil {
		return nil, ss_errs.ErrStreamerNotFound{}
	}
	return st, nil
}
func (self *mock_streamers) FindByName(name string) (*types.Streamer, error) {
	st := self.by_name[name]
	if st == nil {
		return nil, ss_errs.ErrStreamerNotFound{}
	}
	return st, nil
}
func (self *mock_streamers) GetByTag(tag string) ([]*types.Streamer, error) {
	st := self.by_tag[tag]
	if st == nil {
		return []*types.Streamer{}, nil
	}
	return []*types.Streamer{st}, nil
}

func (*mock_streamers) Create(types.NewStreamer) (int, error) { panic("") }
func (*mock_streamers) Remove(int) error                      { panic("") }
func (*mock_streamers) GetAll() ([]*types.Streamer, error)    { panic("") }
func (*mock_streamers) UpdName(int, string, []string) error   { panic("") }
func (*mock_streamers) UpdTg(int, int64) error                { panic("") }
func (*mock_streamers) AddLinks(int, []string) error          { panic("") }
func (*mock_streamers) RemoveLinks(int, []string) error       { panic("") }

func (self *mock_users) GetByLogin(login string) (*types.User, error) {
	usr := self.by_login[login]
	if usr == nil {
		return nil, ss_errs.ErrUserNotFound{}
	}
	return usr, nil
}
func (self *mock_users) GetById(id int64) (*types.User, error) {
	usr := self.by_id[id]
	if usr == nil {
		return nil, ss_errs.ErrUserNotFound{}
	}
	return usr, nil
}

func (*mock_users) CreateOrUpdate(types.User) (*types.User, error) { panic("") }

func make_setup() setup {
	st := &mock_streamers{
		by_id:    map[int]*types.Streamer{},
		by_tg_id: map[int64]*types.Streamer{},
		by_name:  map[string]*types.Streamer{},
		by_tag:   map[string]*types.Streamer{},
	}
	usr := &mock_users{
		by_login: map[string]*types.User{},
		by_id:    map[int64]*types.User{},
	}
	return setup{
		streamers: st,
		users:     usr,
		picker:    streamer_picker.New(st, usr),
	}
}
