package streamer_picker_test

import (
	"stream-scheduler/ss_errs"
	"stream-scheduler/types"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestNotFound(t *testing.T) {
	setup := make_setup()
	_, err := setup.picker.GetStreamer("abc")
	assert.ErrorIs(t, err, ss_errs.ErrStreamerNotFound{})
}

func TestFindByName(t *testing.T) {
	setup := make_setup()
	st := types.Streamer{Id: 123}
	setup.streamers.by_name["abc"] = &st
	got, err := setup.picker.GetStreamer("abc")
	assert.Nil(t, err)
	assert.Equal(t, st, *got)
}

func TestFindById(t *testing.T) {
	setup := make_setup()
	st := types.Streamer{Id: 123}
	st.Name = "abc"
	setup.streamers.by_id[123] = &st
	got, err := setup.picker.GetStreamer("123")
	assert.Nil(t, err)
	assert.Equal(t, st, *got)
}

func TestFindByTag(t *testing.T) {
	setup := make_setup()
	st := types.Streamer{Id: 123}
	st.Name = "abc"
	setup.streamers.by_tag["tag"] = &st
	got, err := setup.picker.GetStreamer("tag")
	assert.Nil(t, err)
	assert.Equal(t, st, *got)
}

func TestFindByLogin(t *testing.T) {
	setup := make_setup()
	st := types.Streamer{Id: 123}
	st.Name = "abc"
	tg_id := int64(2)
	st.TgId = &tg_id
	setup.streamers.by_tg_id[tg_id] = &st
	usr := &types.User{
		TgId:  tg_id,
		Login: "tg_login",
	}
	setup.users.by_login["tg_login"] = usr
	setup.users.by_id[tg_id] = usr
	got, err := setup.picker.GetStreamer("tg_login")
	assert.Nil(t, err)
	assert.Equal(t, st.Id, got.Id)
	assert.Equal(t, st.Name, got.Name)
	assert.Equal(t, "tg_login", *got.TgLogin)
}

func TestHasNoTg(t *testing.T) {
	setup := make_setup()
	st := types.Streamer{Id: 123}
	st.Name = "abc"
	setup.streamers.by_id[123] = &st
	got, err := setup.picker.GetStreamer("123")
	assert.Nil(t, err)
	assert.Nil(t, got.TgLogin)
}

func TestHasTgButNotFound(t *testing.T) {
	setup := make_setup()
	st := types.Streamer{Id: 123}
	tg_id := int64(321)
	st.TgId = &tg_id
	setup.streamers.by_id[123] = &st
	got, err := setup.picker.GetStreamer("123")
	assert.Nil(t, err)
	assert.Nil(t, got.TgLogin)
}

func TestHasTgOk(t *testing.T) {
	setup := make_setup()
	st := types.Streamer{Id: 123}
	tg_id := int64(321)
	st.TgId = &tg_id
	setup.streamers.by_id[123] = &st
	setup.users.by_id[321] = &types.User{Login: "abc"}
	got, err := setup.picker.GetStreamer("123")
	assert.Nil(t, err)
	assert.Equal(t, "abc", *got.TgLogin)
}
