package send_notif_periodic_test

import (
	"fmt"
	"stream-scheduler/periodics/send_notifications"
	"stream-scheduler/proto"
	"stream-scheduler/ss_errs"
	"stream-scheduler/types"
	"sync"
	"time"
)

type mock_events_repo struct {
	mtx    sync.Mutex
	events []*types.EventWithSeconds
	calls  []string
}

type mock_settings_repo struct {
	value types.Settings
}

type mock_streamers_repo struct {
	streamers []types.Streamer
}

type mock_time_serde struct {
	mtx   sync.Mutex
	value string
	calls []time.Time
}

type mock_sender struct {
	mtx   sync.Mutex
	err   map[string]error
	calls []string
}

type mock_now struct {
	time time.Time
}

type setup struct {
	events     *mock_events_repo
	settings   *mock_settings_repo
	streamers  *mock_streamers_repo
	time_serde *mock_time_serde
	sender     *mock_sender
	now        *mock_now
	periodic   proto.IPeriodic
}

func (self *mock_events_repo) GetEventsToNotify() ([]*types.EventWithSeconds, error) {
	return self.events, nil
}

func (self *mock_events_repo) SetSendNotifications(event_id int, seconds_before []int) error {
	self.mtx.Lock()
	defer self.mtx.Unlock()
	call := fmt.Sprintf("%d|%v", event_id, seconds_before)
	self.calls = append(self.calls, call)
	return nil
}

func (*mock_events_repo) Create(types.NewEvent) (int, error)                { panic("") }
func (*mock_events_repo) UpdateInfo(types.UpdEventInfo) error               { panic("") }
func (*mock_events_repo) UpdateTime(int, time.Time) error                   { panic("") }
func (*mock_events_repo) AddNotifTimes(int, []int) error                    { panic("") }
func (*mock_events_repo) GetNotifTimes(int) (map[time.Time]bool, error)     { panic("") }
func (*mock_events_repo) Remove(int) error                                  { panic("") }
func (*mock_events_repo) GetById(int) (*types.Event, error)                 { panic("") }
func (*mock_events_repo) GetChatEvents(int64, bool) ([]*types.Event, error) { panic("") }
func (*mock_events_repo) RemoveOld() error                                  { panic("") }

func (self *mock_settings_repo) Get() (types.Settings, error) {
	return self.value, nil
}

func (*mock_settings_repo) Set(settings types.Settings) error {
	panic("")
}

func (self *mock_streamers_repo) GetById(id int) (*types.Streamer, error) {
	for _, s := range self.streamers {
		if s.Id == id {
			return &s, nil
		}
	}
	return nil, ss_errs.ErrStreamerNotFound{}
}

func (*mock_streamers_repo) Create(types.NewStreamer) (int, error)             { panic("") }
func (*mock_streamers_repo) Remove(int) error                                  { panic("") }
func (*mock_streamers_repo) FindByTg(*int64, *string) (*types.Streamer, error) { panic("") }
func (*mock_streamers_repo) FindByName(string) (*types.Streamer, error)        { panic("") }
func (*mock_streamers_repo) GetAll() ([]*types.Streamer, error)                { panic("") }
func (*mock_streamers_repo) GetByTag(string) ([]*types.Streamer, error)        { panic("") }
func (*mock_streamers_repo) UpdName(int, string, []string) error               { panic("") }
func (*mock_streamers_repo) AddLinks(int, []string) error                      { panic("") }
func (*mock_streamers_repo) RemoveLinks(int, []string) error                   { panic("") }
func (*mock_streamers_repo) UpdTg(int, int64) error                            { panic("") }

func (*mock_time_serde) Parse(src string) (time.Time, error) {
	panic("")
}

func (self *mock_time_serde) Show(t time.Time) (string, error) {
	self.mtx.Lock()
	defer self.mtx.Unlock()
	self.calls = append(self.calls, t)
	return self.value, nil
}

func (self *mock_sender) Send(chat int64, text string) error {
	self.mtx.Lock()
	defer self.mtx.Unlock()
	args := fmt.Sprintf("%d %s", chat, text)
	self.calls = append(self.calls, args)
	return self.err[args]
}

func (self mock_now) Now() time.Time {
	return self.time
}

func make_setup() setup {
	ev := &mock_events_repo{}
	stt := &mock_settings_repo{
		value: types.DefaultSettings(),
	}
	str := &mock_streamers_repo{}
	tm := &mock_time_serde{value: "now"}
	sn := &mock_sender{
		err: map[string]error{},
	}
	now := &mock_now{}
	return setup{
		events:     ev,
		settings:   stt,
		streamers:  str,
		time_serde: tm,
		sender:     sn,
		now:        now,
		periodic:   send_notifications.New(ev, stt, str, tm, sn, now),
	}
}
