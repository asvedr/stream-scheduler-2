package send_notif_periodic_test

import (
	"errors"
	"sort"
	"stream-scheduler/types"
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
)

func make_events() []*types.EventWithSeconds {
	when := time.Date(2020, 1, 1, 0, 0, 0, 0, time.UTC)
	var ev1, ev2 types.EventWithSeconds

	ev1.Id = 1
	ev1.Name = "first event"
	ev1.ChatId = 123
	ev1.StreamerId = nil
	ev1.Time = when
	ev1.Message = "Wee"
	ev1.SecondsBefore = []int{10, 20}

	ev2.Id = 2
	ev2.Name = "second event"
	ev2.ChatId = 123
	ev2.StreamerId = nil
	ev2.Time = when
	ev2.Message = "Woo"
	ev2.SecondsBefore = []int{5}
	return []*types.EventWithSeconds{&ev1, &ev2}
}

func make_event() []*types.EventWithSeconds {
	return make_events()[:1]
}

func TestSendNone(t *testing.T) {
	setup := make_setup()
	setup.events.events = []*types.EventWithSeconds{}
	setup.time_serde.value = "now"
	assert.Nil(t, setup.periodic.Execute())
	assert.Equal(t, 0, len(setup.events.calls))
	assert.Equal(t, 0, len(setup.sender.calls))
}

func TestSendBoth(t *testing.T) {
	setup := make_setup()
	setup.events.events = make_events()
	now := time.Now()
	setup.events.events[0].Time = now

	assert.Nil(t, setup.periodic.Execute())
	sl := sort.StringSlice(setup.events.calls)
	sl.Sort()
	assert.Equal(
		t,
		[]string{"1|[10 20]", "2|[5]"},
		[]string(sl),
	)
	sl = sort.StringSlice(setup.sender.calls)
	sl.Sort()
	assert.Equal(
		t,
		[]string{
			"123 now\nfirst event",
			"123 now\nsecond event",
		},
		[]string(sl),
	)
}

func TestSendOneErr(t *testing.T) {
	setup := make_setup()
	setup.events.events = make_events()
	sender_err := errors.New("oops")
	setup.sender.err["123 now\nfirst event"] = sender_err

	assert.Equal(
		t,
		sender_err,
		setup.periodic.Execute(),
	)
	assert.Equal(
		t,
		[]string{"2|[5]"},
		setup.events.calls,
	)
	sl := sort.StringSlice(setup.sender.calls)
	sl.Sort()
	assert.Equal(
		t,
		[]string{
			"123 now\nfirst event",
			"123 now\nsecond event",
		},
		[]string(sl),
	)
}

func TestStreamerNotFoundLinks(t *testing.T) {
	setup := make_setup()
	setup.events.events = make_event()
	streamer_id := 69
	now := time.Now()
	setup.events.events[0].StreamerId = &streamer_id
	setup.events.events[0].Time = now

	assert.Nil(t, setup.periodic.Execute())
	assert.Equal(
		t,
		[]string{"1|[10 20]"},
		setup.events.calls,
	)
	assert.Equal(
		t,
		[]string{"123 now\nfirst event"},
		setup.sender.calls,
	)
}

func TestSendLinksYes(t *testing.T) {
	setup := make_setup()
	setup.events.events = make_event()
	streamer_id := 69
	now := time.Now()
	setup.events.events[0].StreamerId = &streamer_id
	setup.events.events[0].Time = now
	setup.now.time = now
	var st types.Streamer
	st.Links = []string{"http://link.com/"}
	st.Id = streamer_id
	setup.streamers.streamers = []types.Streamer{st}

	assert.Nil(t, setup.periodic.Execute())
	assert.Equal(
		t,
		[]string{"1|[10 20]"},
		setup.events.calls,
	)
	assert.Equal(
		t,
		[]string{"123 now\nfirst event\nhttp://link.com/"},
		setup.sender.calls,
	)
}

func TestSendLinksNo(t *testing.T) {
	setup := make_setup()
	setup.events.events = make_event()
	streamer_id := 69
	now := time.Now()
	setup.events.events[0].StreamerId = &streamer_id
	setup.events.events[0].Time = now.Add(time.Hour * 10)
	setup.now.time = now
	var st types.Streamer
	st.Links = []string{"http://link.com/"}
	st.Id = streamer_id
	setup.streamers.streamers = []types.Streamer{st}

	assert.Nil(t, setup.periodic.Execute())
	assert.Equal(
		t,
		[]string{"1|[10 20]"},
		setup.events.calls,
	)
	assert.Equal(
		t,
		[]string{"123 now\nfirst event"},
		setup.sender.calls,
	)
}
