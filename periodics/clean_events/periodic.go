package clean_events

import (
	"stream-scheduler/proto"
)

type peridoic struct {
	events_repo proto.IEventsRepo
}

func New(
	events_repo proto.IEventsRepo,
) proto.IPeriodic {
	return peridoic{events_repo: events_repo}
}

func (self peridoic) Execute() error {
	return self.events_repo.RemoveOld()
}
