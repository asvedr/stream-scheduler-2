package send_notifications

import (
	"errors"
	"fmt"
	"stream-scheduler/proto"
	"stream-scheduler/ss_errs"
	"stream-scheduler/types"
	"strings"
	"time"

	"gitlab.com/asvedr/giter"
)

type periodic struct {
	events_repo    proto.IEventsRepo
	settings_repo  proto.ISettingsRepo
	streamers_repo proto.IStreamersRepo
	time_serde     proto.ITimeSerDe
	sender         proto.ISender
	now            proto.INow
}

func New(
	events_repo proto.IEventsRepo,
	settings_repo proto.ISettingsRepo,
	streamers_repo proto.IStreamersRepo,
	time_serde proto.ITimeSerDe,
	sender proto.ISender,
	now proto.INow,
) proto.IPeriodic {
	return &periodic{
		time_serde:     time_serde,
		settings_repo:  settings_repo,
		streamers_repo: streamers_repo,
		events_repo:    events_repo,
		sender:         sender,
		now:            now,
	}
}

func (self *periodic) Execute() error {
	events, err := self.events_repo.GetEventsToNotify()
	if err != nil {
		return err
	}
	if len(events) == 0 {
		return nil
	}
	return giter.GatherMapErr(
		self.send_event,
		events...,
	)
}

func (self *periodic) send_event(
	ev *types.EventWithSeconds,
) error {
	str_time, err := self.time_serde.Show(ev.Time)
	if err != nil {
		return err
	}
	msg := fmt.Sprintf("%s\n%s", str_time, ev.Name)
	var links string
	if ev.Time.Sub(self.now.Now()) <= time.Minute {
		links, err = self.get_links(ev.StreamerId)
		msg += "\n" + links
	}
	if err != nil {
		return err
	}
	msg = strings.TrimSpace(msg)
	err = self.sender.Send(ev.ChatId, msg)
	if err != nil {
		return err
	}
	return self.events_repo.SetSendNotifications(
		ev.Id,
		ev.SecondsBefore,
	)
}

func (self *periodic) get_links(streamer_id *int) (string, error) {
	if streamer_id == nil {
		return "", nil
	}
	streamer, err := self.streamers_repo.GetById(*streamer_id)
	if errors.Is(err, ss_errs.ErrStreamerNotFound{}) {
		return "", nil
	}
	if err != nil {
		return "", err
	}
	return strings.Join(streamer.Links, "\n"), nil
}
