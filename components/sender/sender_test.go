package sender_test

import (
	"errors"
	"stream-scheduler/components/sender"
	"stream-scheduler/types"
	"testing"

	"github.com/asvedr/gotgbot/v2"
	"github.com/stretchr/testify/assert"
)

type bot struct {
	recon_resps []error
	send_resps  []error
}

func (self *bot) Reconnect(opts *gotgbot.BotOpts) error {
	resp := self.recon_resps[0]
	self.recon_resps = self.recon_resps[1:]
	return resp
}

func (self *bot) SendMessage(
	chatId int64,
	text string,
	opts *gotgbot.SendMessageOpts,
) (*gotgbot.Message, error) {
	resp := self.send_resps[0]
	self.send_resps = self.send_resps[1:]
	return nil, resp
}

func TestFailedToSend(t *testing.T) {
	snd := sender.New(
		&types.Config{TgMaxTries: 3},
		&bot{
			recon_resps: []error{nil, nil, nil, nil},
			send_resps: []error{
				errors.New("oops"),
				errors.New("oops"),
				errors.New("oops"),
				nil,
			},
		},
	)
	err := snd.Send(123, "hello")
	assert.NotNil(t, err)
	assert.Equal(t, "oops", err.Error())
}

func TestSucceedToSend(t *testing.T) {
	snd := sender.New(
		&types.Config{TgMaxTries: 3},
		&bot{
			recon_resps: []error{nil},
			send_resps:  []error{nil},
		},
	)
	err := snd.Send(123, "hello")
	assert.Nil(t, err)
}
