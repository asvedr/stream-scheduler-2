package sender

import (
	"stream-scheduler/proto"
	"stream-scheduler/types"
)

type sender struct {
	bot       proto.IBot
	max_tries int
}

func New(
	config *types.Config,
	bot proto.IBot,
) proto.ISender {
	return sender{bot: bot, max_tries: config.TgMaxTries}
}

func (self sender) Send(chat int64, text string) error {
	var err error
	for i := 0; i < self.max_tries; i += 1 {
		_, err = self.bot.SendMessage(chat, text, nil)
		if err == nil {
			return nil
		}
		self.bot.Reconnect(nil)
	}
	return err
}
