package streamers_repo_test

import (
	"stream-scheduler/components/sqlite/streamers_repo"
	"stream-scheduler/proto"
	"stream-scheduler/ss_errs"
	"stream-scheduler/types"
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/asvedr/giter"
	ss "gitlab.com/asvedr/safe_sqlite"
)

func make_repo(t *testing.T) proto.IStreamersRepo {
	db, err := ss.New(":memory:")
	assert.Nil(t, err)
	r, err := streamers_repo.New(db)
	assert.Nil(t, err)
	return r
}

func TestCreateGetRemove(t *testing.T) {
	repo := make_repo(t)
	id, err := repo.Create(types.NewStreamer{
		Name:  "Abc",
		Tags:  giter.V("a", "B", "c"),
		Links: giter.V("1", "2", "3"),
	})
	assert.Nil(t, err)
	st, err := repo.GetById(id)
	assert.Nil(t, err)
	assert.Equal(t, st.Name, "abc")
	assert.Equal(t, st.Tags, giter.V("a", "b", "c"))
	assert.Equal(t, st.Links, giter.V("1", "2", "3"))
	err = repo.Remove(id)
	assert.Nil(t, err)
	_, err = repo.GetById(id)
	assert.IsType(t, ss_errs.ErrStreamerNotFound{}, err)
}

func TestFindByTg(t *testing.T) {
	repo := make_repo(t)
	_, err := repo.Create(types.NewStreamer{
		Name:    "Abc",
		Tags:    giter.V("a", "B", "c"),
		Links:   giter.V("1", "2", "3"),
		TgId:    as_link[int64](123),
		TgLogin: as_link("aaa"),
	})
	assert.Nil(t, err)
	_, err = repo.FindByTg(nil, as_link("xxx"))
	assert.IsType(t, ss_errs.ErrStreamerNotFound{}, err)
	st, err := repo.FindByTg(as_link[int64](123), as_link("aaa"))
	assert.Nil(t, err)
	assert.Equal(t, st.Name, "abc")
	assert.Equal(t, st.Tags, giter.V("a", "b", "c"))
	assert.Equal(t, st.Links, giter.V("1", "2", "3"))
}

func TestFindByName(t *testing.T) {
	repo := make_repo(t)
	_, err := repo.Create(types.NewStreamer{
		Name:    "Abc",
		Tags:    giter.V("a", "B", "c"),
		Links:   giter.V("1", "2", "3"),
		TgId:    as_link[int64](123),
		TgLogin: as_link("aaa"),
	})
	assert.Nil(t, err)
	_, err = repo.FindByName("xxx")
	assert.IsType(t, ss_errs.ErrStreamerNotFound{}, err)
	st, err := repo.FindByName("abc")
	assert.Nil(t, err)
	assert.Equal(t, st.Name, "abc")
	assert.Equal(t, st.Tags, giter.V("a", "b", "c"))
	assert.Equal(t, st.Links, giter.V("1", "2", "3"))
}

func TestGetAll(t *testing.T) {
	repo := make_repo(t)
	repo.Create(types.NewStreamer{Name: "Abc"})
	repo.Create(types.NewStreamer{Name: "Def"})
	all, err := repo.GetAll()
	assert.Nil(t, err)
	assert.Equal(t, len(all), 2)
	assert.Equal(t, []string{"abc", "def"}, names(all))
}

func TestGetByTag(t *testing.T) {
	repo := make_repo(t)
	repo.Create(
		types.NewStreamer{Name: "Abc", Tags: giter.V("a", "b")},
	)
	repo.Create(
		types.NewStreamer{Name: "Def", Tags: giter.V("b", "c")},
	)
	got, err := repo.GetByTag("a")
	assert.Nil(t, err)
	assert.Equal(t, []string{"abc"}, names(got))
	got, err = repo.GetByTag("c")
	assert.Nil(t, err)
	assert.Equal(t, []string{"def"}, names(got))
	got, err = repo.GetByTag("b")
	assert.Nil(t, err)
	assert.Equal(t, []string{"abc", "def"}, names(got))
}

func TestUpdName(t *testing.T) {
	repo := make_repo(t)
	id, _ := repo.Create(
		types.NewStreamer{Name: "Abc", Tags: giter.V("a", "b")},
	)
	repo.Create(
		types.NewStreamer{Name: "Def", Tags: giter.V("b", "c")},
	)
	err := repo.UpdName(id, "xXx", giter.V("c", "d"))
	assert.Nil(t, err)

	got, err := repo.GetByTag("a")
	assert.Nil(t, err)
	assert.Equal(t, 0, len(got))

	got, err = repo.GetByTag("c")
	assert.Equal(
		t,
		[]string{"xxx", "def"},
		names(got),
	)
}

func TestAddRemoveLinks(t *testing.T) {
	repo := make_repo(t)
	id, _ := repo.Create(
		types.NewStreamer{Name: "Abc", Links: []string{"a", "b"}},
	)
	got, _ := repo.GetById(id)
	assert.Equal(t, []string{"a", "b"}, got.Links)
	err := repo.RemoveLinks(id, []string{"a", "c"})
	assert.Nil(t, err)
	got, _ = repo.GetById(id)
	assert.Equal(t, []string{"b"}, got.Links)
	err = repo.AddLinks(id, []string{"c", "d"})
	assert.Nil(t, err)
	got, _ = repo.GetById(id)
	assert.Equal(t, []string{"b", "c", "d"}, got.Links)
}

func TestUpdTg(t *testing.T) {
	repo := make_repo(t)
	id, _ := repo.Create(
		types.NewStreamer{Name: "Abc", Links: []string{"a", "b"}},
	)
	got, _ := repo.GetById(id)
	assert.Nil(t, got.TgId)
	assert.Nil(t, repo.UpdTg(id, 123))
	got, _ = repo.GetById(id)
	assert.Equal(t, *got.TgId, int64(123))
}

// AddLinks(streamer_id int, links []string) error
// RemoveLinks(streamer_id int, links []string) error

func as_link[T any](t T) *T {
	return &t
}

func names(sts []*types.Streamer) []string {
	var result []string
	for _, st := range sts {
		result = append(result, st.Name)
	}
	return result
}
