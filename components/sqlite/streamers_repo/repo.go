package streamers_repo

import (
	"database/sql"
	"fmt"
	"stream-scheduler/components/sqlite/common"
	"stream-scheduler/proto"
	"stream-scheduler/ss_errs"
	"stream-scheduler/types"
	t "stream-scheduler/types"
	"strings"

	"gitlab.com/asvedr/giter"
	ss "gitlab.com/asvedr/safe_sqlite"
)

type Repo struct {
	db ss.Db
}

const q_cr_streamers = `
CREATE TABLE IF NOT EXISTS ss_streamers (
	Id INTEGER PRIMARY KEY,
	Name STRING NOT NULL UNIQUE,
	TgId INTEGER,
	TgLogin STRING
)
`

const q_cr_links = `
CREATE TABLE IF NOT EXISTS ss_links (
	Url STRING PRIMARY KEY,
	StreamerId INTEGER NOT NULL,
	FOREIGN KEY(StreamerId) REFERENCES ss_streamers(Id)
)
`

const q_cr_tags = `
CREATE TABLE IF NOT EXISTS ss_tags (
	Id INTEGER PRIMARY KEY,
	Tag STRING NOT NULL,
	StreamerId INTEGER NOT NULL,
	FOREIGN KEY(StreamerId) REFERENCES ss_streamers(Id)
)
`

func New(db ss.Db) (proto.IStreamersRepo, error) {
	err := common.ExecMany(db, q_cr_streamers, q_cr_links, q_cr_tags)
	return wrap(Repo{db: db}), err
}

func (self Repo) Create(streamer t.NewStreamer) (int, error) {
	var id int
	if streamer.TgLogin != nil {
		lnk := strings.ToLower(*streamer.TgLogin)
		streamer.TgLogin = &lnk
	}
	id64, err := self.db.ExecReturnId(
		"INSERT INTO ss_streamers (Name, TgId, TgLogin) VALUES (?, ?, ?)",
		strings.ToLower(streamer.Name),
		streamer.TgId,
		streamer.TgLogin,
	)
	if err != nil {
		return id, err
	}
	id = int(id64)
	err = add_tags(self.db, id, streamer.Tags)
	if err != nil {
		return id, err
	}
	if len(streamer.Links) > 0 {
		err = self.AddLinks(id, streamer.Links)
		if err != nil {
			return id, err
		}
	}
	return id, nil
}

func (self Repo) Remove(strmr_id int) error {
	sid := fmt.Sprint(strmr_id)
	return self.db.WithLocked(func(db ss.IDb) error {
		return common.ExecMany(
			db,
			"DELETE FROM ss_tags WHERE StreamerId = "+sid,
			"DELETE FROM ss_links WHERE StreamerId = "+sid,
			"DELETE FROM ss_streamers WHERE Id = "+sid,
		)
	})
}

func (self Repo) GetById(id int) (*t.Streamer, error) {
	streamer, err := ss.FetchOne(
		self.db,
		deser_streamer,
		"SELECT Id, Name, TgId, TgLogin FROM ss_streamers WHERE id = ?",
		id,
	)
	return self.get_one_finalizer(streamer, err)
}

func (self Repo) FindByTg(tg_id *int64, tg_login *string) (*t.Streamer, error) {
	if tg_id == nil && tg_login == nil {
		return nil, ss_errs.ErrStreamerNotFound{}
	}
	var cond []string
	var params []any
	if tg_id != nil {
		cond = append(cond, "TgId = ?")
		params = append(params, *tg_id)
	}
	if tg_login != nil {
		cond = append(cond, "TgLogin = ?")
		params = append(params, *tg_login)
	}
	c := strings.Join(cond, " OR ")
	streamer, err := ss.FetchOne(
		self.db,
		deser_streamer,
		"SELECT Id, Name, TgId, TgLogin FROM ss_streamers WHERE "+c,
		params...,
	)
	return self.get_one_finalizer(streamer, err)
}

func (self Repo) FindByName(name string) (*t.Streamer, error) {
	streamer, err := ss.FetchOne(
		self.db,
		deser_streamer,
		"SELECT Id, Name, TgId, TgLogin FROM ss_streamers WHERE Name = ?",
		strings.ToLower(name),
	)
	return self.get_one_finalizer(streamer, err)
}

func (self Repo) GetAll() ([]*t.Streamer, error) {
	streamers, err := ss.FetchSlice(
		self.db,
		deser_streamer,
		"SELECT Id, Name, TgId, TgLogin FROM ss_streamers",
	)
	if err != nil {
		return nil, err
	}
	return streamers, self.enrich_streamers(streamers...)
}

func (self Repo) GetByTag(text string) ([]*t.Streamer, error) {
	streamers, err := ss.FetchSlice(
		self.db,
		deser_streamer,
		`SELECT s.Id, s.Name, s.TgId, s.TgLogin
		FROM ss_streamers AS s
		JOIN ss_tags AS t ON s.Id = t.StreamerId
		WHERE t.Tag = ?
		ORDER BY s.Id
		`,
		strings.ToLower(text),
	)
	if err != nil {
		return nil, err
	}
	return streamers, self.enrich_streamers(streamers...)
}

func (self Repo) UpdName(streamer_id int, name string, tags []string) error {
	return self.db.WithLocked(func(db ss.IDb) error {
		err := db.Exec(
			"UPDATE ss_streamers SET Name = ? WHERE Id = ?",
			lower_any(name),
			streamer_id,
		)
		if err != nil {
			return err
		}
		err = db.Exec(
			"DELETE FROM ss_tags WHERE StreamerId = ?",
			streamer_id,
		)
		if err != nil {
			return err
		}
		return add_tags(db, streamer_id, tags)
	})
}

func (self Repo) AddLinks(streamer_id int, links []string) error {
	if len(links) == 0 {
		return nil
	}
	query := fmt.Sprintf(
		"INSERT INTO ss_links (StreamerId, Url) VALUES %s",
		common.RepeatTmpl("(?,?)", len(links)),
	)
	var params []any
	for _, l := range links {
		params = append(params, streamer_id, lower_any(l))
	}
	return self.db.Exec(query, params...)
}

func (self Repo) RemoveLinks(streamer_id int, links []string) error {
	if len(links) == 0 {
		return nil
	}
	query := fmt.Sprintf(
		"DELETE FROM ss_links WHERE StreamerId = ? AND Url IN (%s)",
		common.RepeatTmpl("?", len(links)),
	)
	params := []any{streamer_id}
	for _, l := range links {
		params = append(params, lower_any(l))
	}
	return self.db.Exec(query, params...)
}

func (self Repo) UpdTg(streamer_id int, tg int64) error {
	query := "UPDATE ss_streamers SET TgId = ? WHERE Id = ?"
	return self.db.Exec(query, tg, streamer_id)
}

func (self Repo) get_one_finalizer(st **types.Streamer, err error) (*types.Streamer, error) {
	if err != nil {
		return nil, err
	}
	if st == nil {
		return nil, ss_errs.ErrStreamerNotFound{}
	}
	return *st, self.enrich_streamers(*st)
}

func (self Repo) enrich_streamers(streamers ...*t.Streamer) error {
	if len(streamers) == 0 {
		return nil
	}
	id_map := giter.V2M(
		func(s *t.Streamer) (int, *t.Streamer) {
			return s.Id, s
		},
		streamers...,
	)
	id_params := giter.Map(common.AsAny, giter.MKeys(id_map)...)
	query := fmt.Sprintf(
		"SELECT StreamerId, Tag FROM ss_tags WHERE StreamerId IN (%s)",
		common.RepeatTmpl("?", len(id_params)),
	)
	iter := func(r *sql.Rows) error {
		var id int
		var tag string
		err := r.Scan(&id, &tag)
		if err != nil {
			return err
		}
		id_map[id].Tags = append(id_map[id].Tags, tag)
		return nil
	}
	err := self.db.IterMany(iter, query, id_params...)
	if err != nil {
		return err
	}
	query = fmt.Sprintf(
		"SELECT StreamerId, Url FROM ss_links WHERE StreamerId IN (%s)",
		common.RepeatTmpl("?", len(id_params)),
	)
	iter = func(r *sql.Rows) error {
		var id int
		var link string
		err := r.Scan(&id, &link)
		if err != nil {
			return err
		}
		id_map[id].Links = append(id_map[id].Links, link)
		return nil
	}
	err = self.db.IterMany(iter, query, id_params...)
	return err
}

func add_tags(db ss.IDb, id int, tags []string) error {
	if len(tags) == 0 {
		return nil
	}
	tmpl := common.RepeatTmpl("(?,?)", len(tags))
	var params []any
	for _, tag := range tags {
		params = append(params, strings.ToLower(tag), id)
	}
	return db.Exec(
		"INSERT INTO ss_tags (Tag, StreamerId) VALUES "+tmpl,
		params...,
	)
}

func deser_streamer(rows *sql.Rows) (*t.Streamer, error) {
	var res t.Streamer
	err := rows.Scan(&res.Id, &res.Name, &res.TgId, &res.TgLogin)
	return &res, err
}

func deser_str(rows *sql.Rows) (string, error) {
	var s string
	err := rows.Scan(&s)
	return s, err
}

func lower_any(s string) any {
	return strings.ToLower(s)
}
