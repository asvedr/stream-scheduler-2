package streamers_repo

import (
	"stream-scheduler/proto"
	"stream-scheduler/ss_errs"
	t "stream-scheduler/types"

	"gitlab.com/asvedr/errmap"
)

type wrapper struct {
	emap errmap.Mapping
	repo proto.IStreamersRepo
}

func wrap(repo proto.IStreamersRepo) proto.IStreamersRepo {
	return &wrapper{
		repo: repo,
		emap: errmap.WithDefault(
			errmap.DefaultF(ss_errs.NewErrSQLError),
			errmap.OnAs[ss_errs.ErrStreamerNotFound]().DoAsIs(),
		),
	}
}

func (self *wrapper) Create(streamer t.NewStreamer) (int, error) {
	val, err := self.repo.Create(streamer)
	return val, self.emap.Map(err)
}

func (self *wrapper) Remove(streamer_id int) error {
	return self.emap.Map(self.repo.Remove(streamer_id))
}

func (self *wrapper) GetById(id int) (*t.Streamer, error) {
	val, err := self.repo.GetById(id)
	return val, self.emap.Map(err)
}

func (self *wrapper) FindByTg(tg_id *int64, tg_login *string) (*t.Streamer, error) {
	val, err := self.repo.FindByTg(tg_id, tg_login)
	return val, self.emap.Map(err)
}

func (self *wrapper) FindByName(name string) (*t.Streamer, error) {
	val, err := self.repo.FindByName(name)
	return val, self.emap.Map(err)
}

func (self *wrapper) GetAll() ([]*t.Streamer, error) {
	val, err := self.repo.GetAll()
	return val, self.emap.Map(err)
}

func (self *wrapper) GetByTag(text string) ([]*t.Streamer, error) {
	val, err := self.repo.GetByTag(text)
	return val, self.emap.Map(err)
}

func (self *wrapper) UpdName(streamer_id int, name string, tags []string) error {
	return self.emap.Map(self.repo.UpdName(streamer_id, name, tags))
}

func (self *wrapper) AddLinks(streamer_id int, links []string) error {
	return self.emap.Map(self.repo.AddLinks(streamer_id, links))
}

func (self *wrapper) UpdTg(streamer_id int, tg int64) error {
	return self.emap.Map(self.repo.UpdTg(streamer_id, tg))
}

func (self *wrapper) RemoveLinks(streamer_id int, links []string) error {
	return self.emap.Map(self.repo.RemoveLinks(streamer_id, links))
}
