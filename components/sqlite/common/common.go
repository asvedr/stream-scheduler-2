package common

import (
	"strings"

	ss "gitlab.com/asvedr/safe_sqlite"
)

func RepeatTmpl(tmpl string, cnt int) string {
	if cnt == 0 {
		return ""
	}
	return tmpl + strings.Repeat(","+tmpl, cnt-1)
}

func AsAny[T any](t T) any {
	return t
}

func ExecMany(db ss.IDb, queries ...string) error {
	for _, q := range queries {
		if err := db.Exec(q); err != nil {
			return nil
		}
	}
	return nil
}

// func ExecIter(db ss.IDb, queries []string, params [][]any) error {
// 	for i, query := range queries {
// 		if err := db.Exec(query, params[i]...); err != nil {
// 			return err
// 		}
// 	}
// 	return nil
// }
