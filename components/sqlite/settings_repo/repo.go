package settings_repo

import (
	"database/sql"
	"errors"
	"fmt"
	"strconv"
	"stream-scheduler/components/sqlite/common"
	"stream-scheduler/proto"
	"stream-scheduler/types"
	t "stream-scheduler/types"
	"strings"

	"gitlab.com/asvedr/giter"
	ss "gitlab.com/asvedr/safe_sqlite"
)

type Repo struct {
	db         ss.IDb
	dflt_value t.Settings
}

const q_cr_table string = `
CREATE TABLE IF NOT EXISTS ss_settings (
	Key string primary key not null,
	Val string not null
)
`

const KeyTimezones = "Timezones"
const KeyDefaultTimezone = "DefaultTimezone"
const KeyNotifShiftsInMinutes = "NotifShiftsInMinutes"
const KeyLang = "Lang"

var InvalidTz = errors.New("invalid tz")

func New(db ss.IDb) (proto.ISettingsRepo, error) {
	err := db.Exec(q_cr_table)
	val := t.DefaultSettings()
	return wrap(&Repo{db: db, dflt_value: val}), err
}

func (self *Repo) Get() (t.Settings, error) {
	f := func(rows *sql.Rows) (string, string, error) {
		var key, val string
		err := rows.Scan(&key, &val)
		return key, val, err
	}
	pairs, err := ss.FetchMap(self.db, f, "SELECT Key, Val FROM ss_settings")
	if err != nil {
		return t.Settings{}, err
	}
	return self.raw_to_val(pairs)
}

func (self *Repo) Set(settings t.Settings) error {
	pairs := val_to_raw(settings)
	tmpl := common.RepeatTmpl("(?,?)", len(pairs))
	query := "INSERT OR REPLACE INTO ss_settings (Key, Val) VALUES" + tmpl
	var params []any
	for key, val := range pairs {
		params = append(params, key, val)
	}
	return self.db.Exec(query, params...)
}

func (self *Repo) raw_to_val(raw map[string]string) (t.Settings, error) {
	var res t.Settings = self.dflt_value
	var found bool
	var val string
	var err error
	if val, found = raw[KeyTimezones]; found {
		res.Timezones, err = parse_timezones(val)
		if err != nil {
			return res, err
		}
	}
	if val, found = raw[KeyDefaultTimezone]; found {
		res.DefaultTimezone = val
	}
	if val, found = raw[KeyNotifShiftsInMinutes]; found {
		res.NotifShiftsInMinutes, err = parse_shifts(val)
	}
	if val, found = raw[KeyLang]; found {
		res.Lang = types.Lang(val)
	}
	return res, err
}

func val_to_raw(val t.Settings) map[string]string {
	return map[string]string{
		KeyTimezones:            dump_timezones(val.Timezones),
		KeyDefaultTimezone:      val.DefaultTimezone,
		KeyNotifShiftsInMinutes: dump_shifts(val.NotifShiftsInMinutes),
		KeyLang:                 string(val.Lang),
	}
}

func parse_timezones(src string) ([]t.Tz, error) {
	val, err := giter.MapErr(parse_tz, strings.Split(src, ",")...)
	if err != nil {
		err = errors.New("invalid tz line: " + src)
	}
	return val, err
}

func parse_tz(src string) (t.Tz, error) {
	split := strings.Split(src, ":")
	if len(split) != 2 {
		return t.Tz{}, InvalidTz
	}
	shift, err := strconv.Atoi(split[1])
	return t.Tz{Name: split[0], Shift: shift}, err
}

func dump_timezones(tzs []t.Tz) string {
	dump := func(tz t.Tz) string {
		return fmt.Sprintf("%s:%d", tz.Name, tz.Shift)
	}
	return strings.Join(giter.Map(dump, tzs...), ",")
}

func parse_shifts(src string) ([]int, error) {
	val, err := giter.MapErr(strconv.Atoi, strings.Split(src, ",")...)
	if err != nil {
		err = errors.New("invalid shifts line: " + src)
	}
	return val, err
}

func dump_shifts(shifts []int) string {
	return strings.Join(giter.Map(strconv.Itoa, shifts...), ",")
}
