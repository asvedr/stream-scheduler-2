package settings_repo

import (
	"stream-scheduler/proto"
	"stream-scheduler/ss_errs"
	t "stream-scheduler/types"

	"gitlab.com/asvedr/errmap"
)

type wrapper struct {
	emap errmap.Mapping
	repo proto.ISettingsRepo
}

func wrap(repo proto.ISettingsRepo) proto.ISettingsRepo {
	return &wrapper{
		repo: repo,
		emap: errmap.WithDefault(
			errmap.DefaultF(ss_errs.NewErrSQLError),
		),
	}
}

func (self *wrapper) Get() (t.Settings, error) {
	val, err := self.repo.Get()
	return val, self.emap.Map(err)
}

func (self *wrapper) Set(settings t.Settings) error {
	return self.emap.Map(self.repo.Set(settings))
}
