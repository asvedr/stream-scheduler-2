package settings_repo_test

import (
	"stream-scheduler/components/sqlite/settings_repo"
	"stream-scheduler/proto"
	"stream-scheduler/types"
	"testing"

	"github.com/stretchr/testify/assert"
	ss "gitlab.com/asvedr/safe_sqlite"
)

func make_repo(t *testing.T) proto.ISettingsRepo {
	db, err := ss.New(":memory:")
	assert.Nil(t, err)
	repo, err := settings_repo.New(db)
	assert.Nil(t, err)
	return repo
}

func TestDefault(t *testing.T) {
	repo := make_repo(t)
	stts, err := repo.Get()
	assert.Nil(t, err)
	assert.Equal(
		t,
		types.Settings{
			Timezones: []types.Tz{
				{Name: "msk", Shift: 3}, {Name: "cy", Shift: 2},
			},
			DefaultTimezone:      "msk",
			NotifShiftsInMinutes: []int{60, 30, 10, 5, 1},
			Lang:                 types.LangRu,
		},
		stts,
	)
}

func TestCustom(t *testing.T) {
	repo := make_repo(t)
	stts := types.Settings{
		Timezones: []types.Tz{
			{Name: "x", Shift: 1},
			{Name: "y", Shift: -1},
			{Name: "z", Shift: 0},
		},
		DefaultTimezone:      "y",
		NotifShiftsInMinutes: []int{1, 2, 3},
		Lang:                 types.LangEn,
	}
	assert.Nil(t, repo.Set(stts))
	got, err := repo.Get()
	assert.Nil(t, err)
	assert.Equal(t, stts, got)
}
