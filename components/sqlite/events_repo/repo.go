package events_repo

import (
	"database/sql"
	"fmt"
	"stream-scheduler/components/sqlite/common"
	"stream-scheduler/proto"
	"stream-scheduler/ss_errs"
	t "stream-scheduler/types"
	"strings"
	"time"

	"gitlab.com/asvedr/giter"
	ss "gitlab.com/asvedr/safe_sqlite"
)

type Repo struct {
	db              ss.Db
	autoremove_time time.Duration
}

const q_cr_ev_table string = `
CREATE TABLE IF NOT EXISTS ss_events (
	Id INTEGER PRIMARY KEY,
	Name STRING NOT NULL,
	ChatId INTEGER NOT NULL,
	StreamerId INTEGER,
	Time INTEGER NOT NULL,
	Message STRING NOT NULL
)`

const q_cr_notif_table string = `
CREATE TABLE IF NOT EXISTS ss_notif_time (
	Id INTEGER PRIMARY KEY,
	EventId INTEGER NOT NULL,
	SecBefore INTEGER NOT NULL,
	Notified BOOLEAN,
	FOREIGN KEY(EventId) REFERENCES ss_events(Id)
)`

func New(
	config *t.Config,
	db ss.Db,
) (proto.IEventsRepo, error) {
	err := common.ExecMany(db, q_cr_ev_table, q_cr_notif_table)
	at := time.Hour * time.Duration(config.RmEventHour)
	return wrap(&Repo{db: db, autoremove_time: at}), err
}

func (self *Repo) Create(event t.NewEvent) (int, error) {
	id, err := self.db.ExecReturnId(
		`INSERT INTO ss_events
		(Name, ChatId, StreamerId, Time, Message)
		VALUES (?, ?, ?, ?, ?)`,
		event.Name,
		event.ChatId,
		event.StreamerId,
		event.Time.Unix(),
		event.Message,
	)
	return int(id), err
}

func (self *Repo) UpdateInfo(upd t.UpdEventInfo) error {
	var q_upd []string
	var params []any
	if upd.Name != nil {
		q_upd = append(q_upd, "Name = ?")
		params = append(params, *upd.Name)
	}
	if upd.ChatId != nil {
		q_upd = append(q_upd, "ChatId = ?")
		params = append(params, *upd.ChatId)
	}
	if upd.StreamerId != nil {
		q_upd = append(q_upd, "StreamerId = ?")
		params = append(params, *upd.StreamerId)
	}
	if upd.Message != nil {
		q_upd = append(q_upd, "Message = ?")
		params = append(params, *upd.Message)
	}
	if len(q_upd) == 0 {
		return nil
	}
	params = append(params, upd.Id)
	query := fmt.Sprintf(
		"UPDATE ss_events SET %s WHERE Id = ?",
		strings.Join(q_upd, ","),
	)
	return self.db.Exec(query, params...)
}

func (self *Repo) UpdateTime(ev_id int, time time.Time) error {
	return self.db.WithLocked(func(db ss.IDb) error {
		err := db.Exec(
			"UPDATE ss_events SET Time = ? WHERE Id = ?",
			time.Unix(),
			ev_id,
		)
		if err != nil {
			return err
		}
		return db.Exec(
			`UPDATE ss_notif_time
			SET Notified = false
			WHERE EventId = ?`,
			ev_id,
		)
	})
}

func (self *Repo) AddNotifTimes(event_id int, seconds_before []int) error {
	tmpl := common.RepeatTmpl("(?,?,?)", len(seconds_before))
	var params []any
	for _, sec := range seconds_before {
		params = append(params, event_id, sec, false)
	}
	return self.db.Exec(
		`INSERT INTO ss_notif_time 
		(EventId, SecBefore, Notified) VALUES `+tmpl,
		params...,
	)
}

func (self *Repo) GetNotifTimes(event_id int) (map[time.Time]bool, error) {
	query := `SELECT e.Time - nt.SecBefore, nt.Notified
		FROM ss_notif_time AS nt JOIN ss_events AS e
			ON nt.EventId = e.Id
		WHERE e.Id = ?`

	deser := func(row *sql.Rows) (time.Time, bool, error) {
		var secs int64
		var notified bool
		err := row.Scan(&secs, &notified)
		tm := time.Unix(secs, 0).UTC()
		return tm, notified, err
	}
	return ss.FetchMap(
		self.db,
		deser,
		query,
		event_id,
	)
}

func (self *Repo) Remove(event_id int) error {
	return self.db.WithLocked(func(db ss.IDb) error {
		err := db.Exec("DELETE FROM ss_notif_time WHERE EventId = ?", event_id)
		if err != nil {
			return err
		}
		return db.Exec("DELETE FROM ss_events WHERE Id = ?", event_id)
	})
}

func (self *Repo) GetById(event_id int) (*t.Event, error) {
	ev, err := ss.FetchOne(
		self.db,
		deser_event,
		`SELECT 
		Id,Name,ChatId,StreamerId,Time,Message
		FROM ss_events
		WHERE Id = ?
		`,
		event_id,
	)
	if ev == nil && err == nil {
		return nil, ss_errs.ErrEventNotFound{}
	}
	return *ev, err
}

func (self *Repo) GetChatEvents(chat_id int64, filter_expired bool) ([]*t.Event, error) {
	cond := "ChatId = ?"
	params := []any{chat_id}
	if filter_expired {
		cond += " AND Time >= ?"
		params = append(params, time.Now().Unix())
	}
	q := `SELECT 
	Id,Name,ChatId,StreamerId,Time,Message
	FROM ss_events
	WHERE ` + cond
	return ss.FetchSlice(self.db, deser_event, q, params...)
}

func (self *Repo) GetEventsToNotify() ([]*t.EventWithSeconds, error) {
	id_to_ev := map[int]*t.EventWithSeconds{}
	iter := func(row *sql.Rows) error {
		var e t.EventWithSeconds
		var sec int
		var tm int64
		err := row.Scan(
			&e.Id, &e.Name, &e.ChatId, &e.StreamerId, &tm, &e.Message, &sec,
		)
		e.Time = time.Unix(tm, 0).UTC()
		if err != nil {
			return err
		}
		ev, found := id_to_ev[e.Id]
		if found {
			ev.SecondsBefore = append(ev.SecondsBefore, sec)
		} else {
			e.SecondsBefore = []int{sec}
			id_to_ev[e.Id] = &e
		}
		return nil
	}
	err := self.db.IterMany(
		iter,
		`SELECT 
		e.Id,e.Name,e.ChatId,e.StreamerId,e.Time,e.Message,t.SecBefore
		FROM ss_events AS e JOIN ss_notif_time AS t
			ON t.EventId = e.Id
		WHERE e.Time - t.SecBefore <= ? AND t.Notified = FALSE
		`,
		time.Now().Unix(),
	)
	return giter.MValues(id_to_ev), err
}

func (self *Repo) SetSendNotifications(event_id int, seconds_before []int) error {
	if len(seconds_before) == 0 {
		return nil
	}
	query := fmt.Sprintf(
		`UPDATE ss_notif_time SET Notified = TRUE
		WHERE EventId = ? AND SecBefore IN (%s)`,
		common.RepeatTmpl("?", len(seconds_before)),
	)
	params := []any{event_id}
	params = append(
		params,
		giter.Map(common.AsAny, seconds_before...)...,
	)
	return self.db.Exec(query, params...)
}

func (self *Repo) RemoveOld() error {
	threshold := time.Now().Add(-self.autoremove_time).Unix()
	return self.db.Exec(
		"DELETE FROM ss_events WHERE Time < ?",
		threshold,
	)
}

func deser_event(rows *sql.Rows) (*t.Event, error) {
	var ev t.Event
	var tm int64
	err := rows.Scan(
		&ev.Id,
		&ev.Name,
		&ev.ChatId,
		&ev.StreamerId,
		&tm,
		&ev.Message,
	)
	ev.Time = time.Unix(tm, 0).UTC()
	return &ev, err
}
