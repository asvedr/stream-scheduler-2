package events_repo_test

import (
	"stream-scheduler/components/sqlite/events_repo"
	"stream-scheduler/proto"
	"stream-scheduler/ss_errs"
	"stream-scheduler/types"
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
	ss "gitlab.com/asvedr/safe_sqlite"
)

func make_repo(t *testing.T) proto.IEventsRepo {
	c := &types.Config{RmEventHour: 1}
	db, err := ss.New(":memory:")
	assert.Nil(t, err)
	repo, err := events_repo.New(c, db)
	assert.Nil(t, err)
	return repo
}

func TestCreateGetRemove(t *testing.T) {
	repo := make_repo(t)
	tm := time.Date(2020, 1, 1, 0, 0, 0, 0, time.UTC)
	ev1 := types.NewEvent{
		Name:    "name",
		ChatId:  -123,
		Time:    tm,
		Message: "msg",
	}
	streamer := 3
	ev2 := types.NewEvent{
		Name:       "name",
		ChatId:     -123,
		StreamerId: &streamer,
		Time:       tm,
		Message:    "msg",
	}
	for _, new_ev := range []types.NewEvent{ev1, ev2} {
		id, err := repo.Create(new_ev)
		assert.Nil(t, err)
		got, err := repo.GetById(id)
		assert.Nil(t, err)
		assert.Equal(t, new_ev, got.NewEvent)
		assert.Nil(t, repo.Remove(id))
		_, err = repo.GetById(id)
		assert.IsType(t, ss_errs.ErrEventNotFound{}, err)
	}
}

func new_event() types.NewEvent {
	tm := time.Date(2020, 1, 1, 0, 0, 0, 0, time.UTC)
	return types.NewEvent{
		Name:    "name",
		ChatId:  -123,
		Time:    tm,
		Message: "msg",
	}
}

func as_link[T any](val T) *T {
	return &val
}

func TestUpdateInfo(t *testing.T) {
	repo := make_repo(t)
	id, err := repo.Create(new_event())
	assert.Nil(t, err)
	err = repo.UpdateInfo(types.UpdEventInfo{
		Id:         id,
		Name:       as_link("n1"),
		ChatId:     as_link(int64(111)),
		StreamerId: as_link(as_link(1)),
		Message:    as_link("m1"),
	})
	assert.Nil(t, err)
	got, err := repo.GetById(id)
	assert.Nil(t, err)
	assert.Equal(t, "n1", got.Name)
	assert.Equal(t, int64(111), got.ChatId)
	assert.Equal(t, as_link(1), got.StreamerId)
	assert.Equal(t, "m1", got.Message)
	err = repo.UpdateInfo(types.UpdEventInfo{
		Id: id, StreamerId: as_link[*int](nil),
	})
	assert.Nil(t, err)
	got, err = repo.GetById(id)
	assert.Nil(t, err)
	assert.Equal(t, "n1", got.Name)
	assert.Equal(t, int64(111), got.ChatId)
	assert.Nil(t, got.StreamerId)
	assert.Equal(t, "m1", got.Message)
}

func TestUpdateTime(t *testing.T) {
	repo := make_repo(t)
	ev := new_event()
	ev.Time = time.Date(2020, 1, 1, 0, 0, 0, 0, time.UTC)
	id, err := repo.Create(ev)
	assert.Nil(t, err)
	tm := time.Date(2021, 1, 1, 0, 0, 0, 0, time.UTC)
	err = repo.UpdateTime(id, tm)
	assert.Nil(t, err)
	got, err := repo.GetById(id)
	assert.Nil(t, err)
	assert.Equal(t, tm, got.Time)
}

func TestAddGetNotifTimes(t *testing.T) {
	repo := make_repo(t)
	ev := new_event()
	id, err := repo.Create(ev)
	assert.Nil(t, err)

	times, err := repo.GetNotifTimes(id)
	assert.Nil(t, err)
	assert.Equal(t, 0, len(times))

	err = repo.AddNotifTimes(id, []int{10, 20})
	assert.Nil(t, err)
	times, err = repo.GetNotifTimes(id)
	assert.Nil(t, err)
	assert.Equal(
		t,
		map[time.Time]bool{
			ev.Time.Add(-time.Second * 10): false,
			ev.Time.Add(-time.Second * 20): false,
		},
		times,
	)
}

func TestAddGetChatEvents(t *testing.T) {
	repo := make_repo(t)
	ev1 := new_event()
	ev1.Name = "ev1"
	ev1.Time = time.Now().Add(time.Hour)
	ev1.ChatId = 123
	ev2 := new_event()
	ev2.Name = "ev2"
	ev2.Time = time.Now().Add(-time.Hour)
	ev2.ChatId = 123
	ev3 := new_event()
	ev3.Name = "ev3"
	ev3.Time = time.Now().Add(time.Hour)
	ev3.ChatId = 321
	_, err := repo.Create(ev1)
	assert.Nil(t, err)
	_, err = repo.Create(ev2)
	assert.Nil(t, err)
	_, err = repo.Create(ev3)
	assert.Nil(t, err)

	get_names := func(evs []*types.Event) map[string]int {
		res := map[string]int{}
		for _, ev := range evs {
			res[ev.Name] = 1
		}
		return res
	}

	evs, err := repo.GetChatEvents(123, false)
	assert.Nil(t, err)
	exp := map[string]int{"ev1": 1, "ev2": 1}
	assert.Equal(t, exp, get_names(evs))

	evs, err = repo.GetChatEvents(123, true)
	assert.Nil(t, err)
	exp = map[string]int{"ev1": 1}
	assert.Equal(t, exp, get_names(evs))
}

func TestSetSendNotifications(t *testing.T) {
	repo := make_repo(t)
	ev := new_event()
	ev.Time = time.Now().Add(time.Minute * 30)
	id, err := repo.Create(ev)
	assert.Nil(t, err)

	evs, err := repo.GetEventsToNotify()
	assert.Nil(t, err)
	assert.Equal(t, 0, len(evs))

	err = repo.AddNotifTimes(id, []int{60 * 10})
	assert.Nil(t, err)
	evs, err = repo.GetEventsToNotify()
	assert.Nil(t, err)
	assert.Equal(t, 0, len(evs))

	err = repo.AddNotifTimes(id, []int{60 * 60})
	assert.Nil(t, err)
	evs, err = repo.GetEventsToNotify()
	assert.Nil(t, err)
	assert.Equal(t, 1, len(evs))
	assert.Equal(t, []int{60 * 60}, evs[0].SecondsBefore)
	assert.Equal(t, id, evs[0].Id)
	ev.Time = time.Unix(ev.Time.Unix(), 0).UTC()
	assert.Equal(t, ev, evs[0].NewEvent)

	err = repo.SetSendNotifications(id, []int{60 * 60})
	assert.Nil(t, err)

	evs, err = repo.GetEventsToNotify()
	assert.Nil(t, err)
	assert.Equal(
		t,
		[]*types.EventWithSeconds{},
		evs,
	)
}

func TestRemoveOld(t *testing.T) {
	repo := make_repo(t)
	ev := new_event()
	ev.Time = time.Now().Add(-time.Minute * 10)
	id1, err := repo.Create(ev)
	assert.Nil(t, err)
	ev = new_event()
	ev.Time = time.Now().Add(-time.Minute * 70)
	id2, err := repo.Create(ev)
	assert.Nil(t, err)
	_, err = repo.GetById(id1)
	assert.Nil(t, err)
	_, err = repo.GetById(id2)
	assert.Nil(t, err)

	assert.Nil(t, repo.RemoveOld())

	_, err = repo.GetById(id1)
	assert.Nil(t, err)
	_, err = repo.GetById(id2)
	assert.ErrorIs(t, err, ss_errs.ErrEventNotFound{})
}
