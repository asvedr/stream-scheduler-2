package events_repo

import (
	"stream-scheduler/proto"
	"stream-scheduler/ss_errs"
	t "stream-scheduler/types"
	"time"

	"gitlab.com/asvedr/errmap"
)

type wrapper struct {
	emap errmap.Mapping
	repo proto.IEventsRepo
}

func wrap(repo proto.IEventsRepo) proto.IEventsRepo {
	return &wrapper{
		repo: repo,
		emap: errmap.WithDefault(
			errmap.DefaultF(ss_errs.NewErrSQLError),
			errmap.OnAs[ss_errs.ErrEventNotFound]().DoAsIs(),
		),
	}
}

func (self *wrapper) Create(event t.NewEvent) (int, error) {
	val, err := self.repo.Create(event)
	return val, self.emap.Map(err)
}

func (self *wrapper) UpdateInfo(u t.UpdEventInfo) error {
	return self.emap.Map(self.repo.UpdateInfo(u))
}

func (self *wrapper) UpdateTime(event_id int, when time.Time) error {
	return self.emap.Map(self.repo.UpdateTime(event_id, when))
}

func (self *wrapper) AddNotifTimes(event_id int, seconds_before []int) error {
	return self.emap.Map(self.repo.AddNotifTimes(event_id, seconds_before))
}

func (self *wrapper) GetNotifTimes(event_id int) (map[time.Time]bool, error) {
	val, err := self.repo.GetNotifTimes(event_id)
	return val, self.emap.Map(err)
}

func (self *wrapper) Remove(event_id int) error {
	return self.emap.Map(self.repo.Remove(event_id))
}

func (self *wrapper) GetById(event_id int) (*t.Event, error) {
	val, err := self.repo.GetById(event_id)
	return val, self.emap.Map(err)
}

func (self *wrapper) GetChatEvents(chat_id int64, filter_expired bool) ([]*t.Event, error) {
	val, err := self.repo.GetChatEvents(chat_id, filter_expired)
	return val, self.emap.Map(err)
}

func (self *wrapper) GetEventsToNotify() ([]*t.EventWithSeconds, error) {
	val, err := self.repo.GetEventsToNotify()
	return val, self.emap.Map(err)
}

func (self *wrapper) SetSendNotifications(event_id int, seconds_before []int) error {
	return self.emap.Map(self.repo.SetSendNotifications(event_id, seconds_before))
}

func (self *wrapper) RemoveOld() error {
	return self.emap.Map(self.repo.RemoveOld())
}
