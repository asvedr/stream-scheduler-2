package users_repo

import (
	"database/sql"
	"reflect"
	"stream-scheduler/proto"
	"stream-scheduler/ss_errs"
	"stream-scheduler/types"
	"strings"

	ss "gitlab.com/asvedr/safe_sqlite"
)

type Repo struct {
	config *types.Config
	db     ss.Db
}

const q_cr_table = `
CREATE TABLE IF NOT EXISTS ss_users (
	TgId INTEGER PRIMARY KEY,
	Login STRING NOT NULL,
	Name STRING NOT NULL
)`

func New(
	config *types.Config,
	db ss.Db,
) (proto.IUsersRepo, error) {
	err := db.Exec(q_cr_table)
	return wrap(&Repo{config: config, db: db}), err
}

func (self *Repo) CreateOrUpdate(fresh types.User) (*types.User, error) {
	fresh.Login = strings.ToLower(fresh.Login)
	result := &fresh
	err := self.db.WithLocked(func(db ss.IDb) error {
		old, err := ss.FetchOne(
			db,
			self.deser,
			"SELECT TgId,Login,Name FROM ss_users WHERE TgId = ?",
			result.TgId,
		)
		if err != nil {
			return err
		}
		if old == nil {
			return insert(db, result)
		}
		if len(result.Login) == 0 {
			result.Login = old.Login
		}
		if len(result.Name) == 0 {
			result.Name = old.Name
		}
		if !reflect.DeepEqual(old, result) {
			return insert(db, result)
		}
		return nil
	})
	self.enrich(result)
	return result, err
}

func (self *Repo) GetById(id int64) (*types.User, error) {
	usr, err := ss.FetchOne(
		self.db,
		self.deser,
		"SELECT TgId,Login,Name FROM ss_users WHERE TgId = ?",
		id,
	)
	if usr == nil && err == nil {
		return nil, ss_errs.ErrUserNotFound{}
	}
	return usr, err
}

func (self *Repo) GetByLogin(login string) (*types.User, error) {
	usr, err := ss.FetchOne(
		self.db,
		self.deser,
		"SELECT TgId,Login,Name FROM ss_users WHERE Login = ?",
		login,
	)
	if usr == nil && err == nil {
		return nil, ss_errs.ErrUserNotFound{}
	}
	return usr, err
}

func (self *Repo) deser(rows *sql.Rows) (types.User, error) {
	var usr types.User
	err := rows.Scan(&usr.TgId, &usr.Login, &usr.Name)
	self.enrich(&usr)
	return usr, err
}

func (self *Repo) enrich(usr *types.User) {
	usr.IsAdmin = usr.TgId == self.config.AdminId
}

func insert(db ss.IDb, usr *types.User) error {
	return db.Exec(
		`INSERT OR REPLACE INTO ss_users
		(TgId,Login,Name) VALUES (?,?,?)`,
		usr.TgId,
		usr.Login,
		usr.Name,
	)
}
