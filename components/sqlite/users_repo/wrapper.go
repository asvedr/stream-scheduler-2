package users_repo

import (
	"stream-scheduler/proto"
	"stream-scheduler/ss_errs"
	"stream-scheduler/types"

	"gitlab.com/asvedr/errmap"
)

type wrapper struct {
	emap errmap.Mapping
	repo proto.IUsersRepo
}

func wrap(repo proto.IUsersRepo) proto.IUsersRepo {
	return &wrapper{
		repo: repo,
		emap: errmap.WithDefault(
			errmap.DefaultF(ss_errs.NewErrSQLError),
			errmap.OnAs[ss_errs.ErrUserNotFound]().DoAsIs(),
		),
	}
}

func (self *wrapper) CreateOrUpdate(u types.User) (*types.User, error) {
	val, err := self.repo.CreateOrUpdate(u)
	return val, self.emap.Map(err)
}

func (self *wrapper) GetById(id int64) (*types.User, error) {
	val, err := self.repo.GetById(id)
	return val, self.emap.Map(err)
}

func (self *wrapper) GetByLogin(login string) (*types.User, error) {
	val, err := self.repo.GetByLogin(login)
	return val, self.emap.Map(err)
}
