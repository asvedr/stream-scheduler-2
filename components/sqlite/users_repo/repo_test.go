package users_repo_test

import (
	"stream-scheduler/components/sqlite/users_repo"
	"stream-scheduler/proto"
	"stream-scheduler/ss_errs"
	"stream-scheduler/types"
	"testing"

	"github.com/stretchr/testify/assert"
	ss "gitlab.com/asvedr/safe_sqlite"
)

func make_repo(t *testing.T) proto.IUsersRepo {
	db, err := ss.New(":memory:")
	assert.Nil(t, err)
	r, err := users_repo.New(&types.Config{AdminId: 123}, db)
	assert.Nil(t, err)
	return r
}

func TestCreate(t *testing.T) {
	repo := make_repo(t)
	usr, err := repo.CreateOrUpdate(types.User{
		TgId:  123,
		Login: "login",
	})
	assert.Nil(t, err)
	assert.True(t, usr.IsAdmin)
	assert.Equal(t, usr.TgId, int64(123))
	assert.Equal(t, usr.Login, "login")
	assert.Equal(t, usr.Name, "")
	usr, err = repo.CreateOrUpdate(types.User{
		TgId:  321,
		Login: "ll",
		Name:  "N",
	})
	assert.Nil(t, err)
	assert.False(t, usr.IsAdmin)
	assert.Equal(t, usr.TgId, int64(321))
	assert.Equal(t, usr.Login, "ll")
	assert.Equal(t, usr.Name, "N")
}

func TestGetById(t *testing.T) {
	repo := make_repo(t)
	repo.CreateOrUpdate(types.User{TgId: 123, Login: "login"})
	repo.CreateOrUpdate(types.User{
		TgId:  321,
		Login: "ll",
		Name:  "N",
	})
	_, err := repo.GetById(1)
	assert.IsType(t, ss_errs.ErrUserNotFound{}, err)
	usr, err := repo.GetById(123)
	assert.Nil(t, err)
	assert.True(t, usr.IsAdmin)
	assert.Equal(t, usr.TgId, int64(123))
	assert.Equal(t, usr.Login, "login")
	assert.Equal(t, usr.Name, "")
	usr, err = repo.GetById(321)
	assert.Nil(t, err)
	assert.False(t, usr.IsAdmin)
	assert.Equal(t, usr.TgId, int64(321))
	assert.Equal(t, usr.Login, "ll")
	assert.Equal(t, usr.Name, "N")
}

func TestGetByLogin(t *testing.T) {
	repo := make_repo(t)
	repo.CreateOrUpdate(types.User{TgId: 123, Login: "login"})
	repo.CreateOrUpdate(types.User{
		TgId:  321,
		Login: "ll",
		Name:  "N",
	})
	_, err := repo.GetByLogin("abc")
	assert.IsType(t, ss_errs.ErrUserNotFound{}, err)
	usr, err := repo.GetByLogin("login")
	assert.Nil(t, err)
	assert.True(t, usr.IsAdmin)
	assert.Equal(t, usr.TgId, int64(123))
	assert.Equal(t, usr.Login, "login")
	assert.Equal(t, usr.Name, "")
	usr, err = repo.GetByLogin("ll")
	assert.Nil(t, err)
	assert.False(t, usr.IsAdmin)
	assert.Equal(t, usr.TgId, int64(321))
	assert.Equal(t, usr.Login, "ll")
	assert.Equal(t, usr.Name, "N")
}

func TestUpdate(t *testing.T) {
	repo := make_repo(t)
	repo.CreateOrUpdate(types.User{TgId: 123, Login: "login"})
	repo.CreateOrUpdate(types.User{
		TgId:  321,
		Login: "ll",
		Name:  "N",
	})
	usr, err := repo.CreateOrUpdate(types.User{TgId: 123, Name: "M"})
	assert.Nil(t, err)
	assert.Equal(t, usr.TgId, int64(123))
	assert.Equal(t, usr.Login, "login")
	assert.Equal(t, usr.Name, "M")
}
