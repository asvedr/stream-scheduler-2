package time_serde

import (
	"stream-scheduler/proto"
	"stream-scheduler/ss_errs"
	"stream-scheduler/types"
	"strings"
	"time"

	"gitlab.com/asvedr/giter"
	gp "gitlab.com/asvedr/gparser"
)

type serde struct {
	now           proto.INow
	settings_repo proto.ISettingsRepo
	parser        gp.Parser[time.Time, *ctx]
}

func New(
	now proto.INow,
	settings_repo proto.ISettingsRepo,
) proto.ITimeSerDe {
	return serde{
		now:           now,
		settings_repo: settings_repo,
		parser:        make_parser(),
	}
}

func (self serde) Parse(src string) (time.Time, error) {
	settings, err := self.settings_repo.Get()
	var result time.Time
	if err != nil {
		return result, err
	}
	context := &ctx{
		now: self.now.Now(),
		tzs: giter.V2M(
			func(tz types.Tz) (string, int) {
				return tz.Name, tz.Shift
			},
			settings.Timezones...,
		),
		default_tz: settings.DefaultTimezone,
		lang:       settings.Lang,
	}
	parsed, rest, err := self.parser.ParseAndRest(src, context)
	if err != nil {
		return result, err
	}
	if strings.TrimSpace(rest) != "" {
		return result, ss_errs.ErrInvalidDateTime
	}
	return *parsed, err
}

func (self serde) Show(tm time.Time) (string, error) {
	now := self.now.Now()
	settings, err := self.settings_repo.Get()
	if err != nil {
		return "", err
	}
	tz := settings.Timezones
	str := renderer{}.render(tm, now, tz, settings.Lang)
	return str, nil
}
