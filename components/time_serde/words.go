package time_serde

import "stream-scheduler/types"

var word_today = map[types.Lang]string{
	types.LangRu: "сегодня",
	types.LangEn: "today",
}

var word_tomorrow = map[types.Lang]string{
	types.LangRu: "завтра",
	types.LangEn: "tomorrow",
}

var word_after = map[types.Lang]string{
	types.LangRu: "через",
	types.LangEn: "after",
}

var word_now = map[types.Lang]string{
	types.LangRu: "сейчас",
	types.LangEn: "now",
}

var word_hour = map[types.Lang]string{
	types.LangRu: "ч",
	types.LangEn: "h",
}

var word_min = map[types.Lang]string{
	types.LangRu: "м",
	types.LangEn: "m",
}

var tokens_min = []string{
	"minutes", "minute", "minut", "mins", "min",
	"минуты", "минуту", "минут", "мин",
}
