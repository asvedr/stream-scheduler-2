package time_serde

import (
	"fmt"
	"stream-scheduler/types"
	"strings"
	"time"

	"gitlab.com/asvedr/giter"
)

const max_diff_to_short = time.Hour * 3

type renderer struct{}

func (self renderer) render(
	tm time.Time,
	now time.Time,
	tzs []types.Tz,
	lang types.Lang,
) string {
	if tm.Sub(now) <= max_diff_to_short {
		return self.render_short_time(tm, now, lang)
	}
	lines := giter.Map(
		func(tz types.Tz) string {
			d := self.render_date(tm, now, lang)
			t := self.render_full_time(tm, now, tz)
			return d + " " + t + " " + tz.Name
		},
		tzs...,
	)
	return strings.Join(lines, "\n")
}

func (self renderer) render_date(
	tm time.Time,
	now time.Time,
	lang types.Lang,
) string {
	days_diff := self.days_diff(tm, now)
	if days_diff == 0 {
		return word_today[lang]
	}
	if days_diff == 1 {
		return word_tomorrow[lang]
	}
	return fmt.Sprintf(
		"%02d.%02d.%4d",
		tm.Day(),
		int(tm.Month()),
		tm.Year(),
	)
}

func (self renderer) days_diff(tm time.Time, now time.Time) int {
	diff := self.cut_time(tm).Sub(self.cut_time(now))
	return int(diff.Hours()) / 24
}

func (renderer) cut_time(tm time.Time) time.Time {
	y, m, d := tm.Date()
	return time.Date(y, m, d, 0, 0, 0, 0, time.UTC)
}

func (self renderer) render_short_time(tm time.Time, now time.Time, lang types.Lang) string {
	diff := tm.Sub(now)
	total_mins := int(diff.Minutes())
	hours := total_mins / 60
	mins := total_mins - (hours * 60)
	var tokens []string
	var token string
	if hours > 0 {
		token = fmt.Sprintf("%d%s", hours, word_hour[lang])
		tokens = append(tokens, token)
	}
	if mins > 0 {
		token = fmt.Sprintf("%d%s", mins, word_min[lang])
		tokens = append(tokens, token)
	}
	if len(tokens) == 0 {
		return word_now[lang]
	}
	return word_after[lang] + " " + strings.Join(tokens, " ")
}

func (self renderer) render_full_time(tm time.Time, now time.Time, tz types.Tz) string {
	local := tm.Add(time.Hour * time.Duration(tz.Shift))
	return fmt.Sprintf("%d:%d", local.Hour(), local.Minute())
}
