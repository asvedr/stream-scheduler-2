package time_serde

import (
	"strconv"
	"stream-scheduler/ss_errs"
	"stream-scheduler/types"
	"time"

	"gitlab.com/asvedr/giter"
	gp "gitlab.com/asvedr/gparser"
	"gitlab.com/asvedr/gparser/build"
	"gitlab.com/asvedr/gparser/join"
	"gitlab.com/asvedr/gparser/preset"
)

const min_year = 1970
const max_year = 3000

type t_ymd struct {
	y int
	m int
	d int
}

type t_time struct {
	rel_time *time.Time
	h        int
	m        int
	date     *t_ymd
	tz       *string
}

type ctx struct {
	now        time.Time
	tzs        map[string]int
	default_tz string
	lang       types.Lang
}

func (self t_ymd) to_date() (time.Time, error) {
	if self.y < min_year || self.y > max_year {
		return time.Time{}, ss_errs.ErrInvalidDateTime
	}
	if self.m < int(time.January) || self.m > int(time.December) {
		return time.Time{}, ss_errs.ErrInvalidDateTime
	}
	if self.d < 1 || self.d > self.max_d() {
		return time.Time{}, ss_errs.ErrInvalidDateTime
	}
	return time.Date(
		self.y,
		time.Month(self.m),
		self.d,
		0, 0, 0, 0, time.UTC,
	), nil
}

func (self t_ymd) max_d() int {
	m := time.Month(self.m)
	var next_month time.Time
	if m == time.December {
		next_month = time.Date(self.y+1, 1, 0, 0, 0, 0, 0, time.UTC)
	} else {
		next_month = time.Date(self.y, m+1, 1, 0, 0, 0, 0, time.UTC)
	}
	return next_month.Add(-time.Hour * time.Duration(24)).Day()
}

func make_parser() gp.Parser[time.Time, *ctx] {
	num := gp.MapErrNoCtx(
		join.SpacePref(preset.Number[*ctx]()),
		strconv.Atoi,
	)
	p_dt := parser_date(num)
	add_tz := func(tm t_time, tz string) t_time {
		tm.tz = &tz
		return tm
	}
	p_tm_dig := parser_time_digit(num)
	p_tz := join.SpacePref(parser_tz())
	p_tm := join.Or(
		join.And(p_tm_dig, p_tz, add_tz),
		join.And(p_tz, p_tm_dig, build.Flip(add_tz)),
		p_tm_dig,
	)
	combine := func(d t_ymd, tm t_time) t_time {
		tm.date = &d
		return tm
	}
	p_t_time := join.Or(
		join.And(p_dt, p_tm, combine),
		join.And(p_tm, p_dt, build.Flip(combine)),
		parser_time_relative(num),
		p_tm,
	)
	return gp.MapErr(p_t_time, t_time_to_time)
}

func parser_date(num gp.Parser[int, *ctx]) gp.Parser[t_ymd, *ctx] {
	sep := join.SpacePref(preset.Token[*ctx](".", "-", "_"))
	return join.Or(
		parser_date_today(),
		parser_date_tomorrow(),
		parser_date_dmy(num, sep),
		parser_date_dm(num, sep),
	)
}

func parser_date_today() gp.Parser[t_ymd, *ctx] {
	p := join.SpacePref(
		preset.Token[*ctx](giter.MValues(word_today)...),
	)
	return gp.Map(
		p,
		func(_ string, ctx *ctx) t_ymd {
			y, m, d := ctx.now.Date()
			return t_ymd{y: y, m: int(m), d: d}
		},
	)
}

func parser_date_tomorrow() gp.Parser[t_ymd, *ctx] {
	p := join.SpacePref(
		preset.Token[*ctx](giter.MValues(word_tomorrow)...),
	)
	return gp.Map(
		p,
		func(_ string, ctx *ctx) t_ymd {
			h24 := time.Hour * 24
			y, m, d := ctx.now.Add(h24).Date()
			return t_ymd{y: y, m: int(m), d: d}
		},
	)
}

func parser_date_dmy(
	num gp.Parser[int, *ctx],
	sep gp.Parser[string, *ctx],
) gp.Parser[t_ymd, *ctx] {
	return join.And5(
		num, sep, num, sep, num,
		func(d int, _ string, m int, _ string, y int) t_ymd {
			return t_ymd{d: d, m: m, y: y}
		},
	)
}

func parser_date_dm(
	num gp.Parser[int, *ctx],
	sep gp.Parser[string, *ctx],
) gp.Parser[t_ymd, *ctx] {
	p := join.And3(
		num, sep, num,
		func(d int, _ string, m int) t_ymd {
			return t_ymd{d: d, m: m}
		},
	)
	add_y := func(dt t_ymd, ctx *ctx) t_ymd {
		dt.y = ctx.now.Year()
		return dt
	}
	return gp.Map(p, add_y)
}

func parser_time_digit(num gp.Parser[int, *ctx]) gp.Parser[t_time, *ctx] {
	return join.And3(
		num,
		join.SpacePref(preset.Token[*ctx](":")),
		num,
		func(h int, _ string, m int) t_time {
			return t_time{h: h, m: m}
		},
	)
}

func parser_time_relative(
	num gp.Parser[int, *ctx],
) gp.Parser[t_time, *ctx] {
	p_token_after := preset.Token[*ctx](giter.MValues(word_after)...)
	p_token_min := preset.Token[*ctx](tokens_min...)
	p_min := join.And3(
		join.SpacePref(p_token_after),
		num,
		join.SpacePref(p_token_min),
		func(_ string, num int, _ string) int { return num },
	)
	maker := func(min int, ctx *ctx) t_time {
		tm := ctx.now.Add(time.Minute * time.Duration(min))
		return t_time{rel_time: &tm}
	}
	return gp.Map(p_min, maker)
}

func parser_tz() gp.Parser[string, *ctx] {
	alphabet := "abcdefghijklmnopqrstuvwxyz"
	return gp.MapErr(
		preset.Word[*ctx]("tz", alphabet),
		func(tz string, ctx *ctx) (string, error) {
			_, found := ctx.tzs[tz]
			if !found {
				return "", ss_errs.ErrInvalidDateTime
			}
			return tz, nil
		},
	)
}

func t_time_to_time(tm t_time, c *ctx) (time.Time, error) {
	if tm.rel_time != nil {
		return *tm.rel_time, nil
	}
	var date time.Time = c.now
	var err error
	if tm.date != nil {
		date, err = tm.date.to_date()
		if err != nil {
			return date, err
		}
	}
	result := time.Date(
		date.Year(),
		date.Month(),
		date.Day(),
		tm.h,
		tm.m,
		0,
		0,
		time.UTC,
	)
	shift := c.tzs[c.default_tz]
	if tm.tz != nil {
		shift = c.tzs[*tm.tz]
	}
	return result.Add(-time.Hour * time.Duration(shift)), nil
}
