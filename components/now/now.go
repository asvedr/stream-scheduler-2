package now

import (
	"stream-scheduler/proto"
	"time"
)

type now struct{}

func New() proto.INow {
	return now{}
}

func (now) Now() time.Time {
	return time.Now().UTC()
}
