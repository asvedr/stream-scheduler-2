package streamer_picker

import (
	"errors"
	"strconv"
	"stream-scheduler/proto"
	"stream-scheduler/ss_errs"
	"stream-scheduler/types"
	"strings"
)

type picker struct {
	streamers_repo proto.IStreamersRepo
	users_repo     proto.IUsersRepo
}

func New(
	streamers_repo proto.IStreamersRepo,
	users_repo proto.IUsersRepo,
) proto.IStreamerPicker {
	return picker{
		streamers_repo: streamers_repo,
		users_repo:     users_repo,
	}
}

func (self picker) GetStreamer(src string) (*types.Streamer, error) {
	src = strings.ToLower(strings.TrimSpace(src))
	id, err := strconv.Atoi(src)
	var streamer *types.Streamer
	if err == nil {
		streamer, err = self.streamers_repo.GetById(id)
	} else {
		streamer, err = self.pick_by_name(src)
	}
	if err == nil {
		return self.enrich(streamer)
	}
	if errors.Is(err, ss_errs.ErrUserNotFound{}) {
		err = ss_errs.ErrStreamerNotFound{}
	}
	return nil, err
}

func (self picker) pick_by_name(src string) (*types.Streamer, error) {
	streamer, err := self.streamers_repo.FindByName(src)
	if err == nil {
		return streamer, err
	}
	if !errors.Is(err, ss_errs.ErrStreamerNotFound{}) {
		return nil, err
	}
	streamers, err := self.streamers_repo.GetByTag(src)
	if err != nil {
		return nil, err
	}
	if len(streamers) > 0 {
		return streamers[0], nil
	}
	usr, err := self.users_repo.GetByLogin(src)
	if err != nil {
		return nil, err
	}
	return self.streamers_repo.FindByTg(&usr.TgId, nil)
}

func (self picker) enrich(streamer *types.Streamer) (*types.Streamer, error) {
	if streamer.TgId == nil {
		return streamer, nil
	}
	usr, err := self.users_repo.GetById(*streamer.TgId)
	if err != nil {
		return streamer, nil
	}
	streamer.TgLogin = usr.GetAnyTgAttr()
	return streamer, nil
}
