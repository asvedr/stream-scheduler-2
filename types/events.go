package types

import "time"

type NewEvent struct {
	Name       string
	ChatId     int64
	StreamerId *int
	Time       time.Time
	Message    string
}

type Event struct {
	NewEvent
	Id int
}

type UpdEventInfo struct {
	Id         int
	Name       *string
	ChatId     *int64
	StreamerId **int
	Message    *string
}

type EventWithSeconds struct {
	Event
	SecondsBefore []int
}
