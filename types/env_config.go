package types

type Config struct {
	AdminId     int64    `descr:"admin tg id"`
	Token       string   `descr:"telegram token"`
	DbPath      string   `descr:"path to db" default:"./ss2.db"`
	RmEventHour int      `descr:"automaticly remove event in N hous after expiration" default:"1"`
	TgMaxTries  int      `descr:"max try to send msg" default:"10"`
	RobotLogin  string   `descr:"robot login"`
	Meta        struct{} `prefix:"STREAM_SCHEDULER_"`
}
