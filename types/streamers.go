package types

import "strings"

type NewStreamer struct {
	Name    string
	TgLogin *string
	TgId    *int64
	Tags    []string
	Links   []string
}

type Streamer struct {
	NewStreamer
	Id int
}

func (self *Streamer) uppercase_name() string {
	runes := []rune(self.Name)
	if len(runes) == 0 {
		return ""
	}
	res := strings.ToUpper(string(runes[0]))
	return res + string(runes[1:])
}

func (self *Streamer) Show() string {
	result := self.uppercase_name() + ":\n"
	for _, link := range self.Links {
		result += "  " + link + "\n"
	}
	return result
}
