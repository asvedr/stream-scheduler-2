package types

import "fmt"

type User struct {
	TgId    int64
	Login   string
	Name    string
	IsAdmin bool
}

func (self *User) GetAnyTgAttr() *string {
	if self.Login != "" {
		return &self.Login
	}
	if self.Name != "" {
		return &self.Name
	}
	str_id := fmt.Sprintf("<tg_id=%d>", self.TgId)
	return &str_id
}
