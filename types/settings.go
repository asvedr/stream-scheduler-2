package types

import (
	"gitlab.com/asvedr/giter"
)

type Tz struct {
	Name  string
	Shift int
}

type Lang string

const (
	LangRu = "ru"
	LangEn = "en"
)

var LangAll = map[Lang]int{LangRu: 1, LangEn: 1}

type Settings struct {
	Timezones            []Tz
	DefaultTimezone      string
	NotifShiftsInMinutes []int
	Lang                 Lang
}

func DefaultSettings() Settings {
	tzs := []Tz{{Name: "msk", Shift: 3}, {Name: "cy", Shift: 2}}
	return Settings{
		Timezones:            tzs,
		DefaultTimezone:      "msk",
		NotifShiftsInMinutes: []int{60, 30, 10, 5, 1},
		Lang:                 LangRu,
	}
}

func (self *Settings) SetTz(name string, shift int) {
	for i, tz := range self.Timezones {
		if tz.Name == name {
			tz.Shift = shift
			self.Timezones[i] = tz
			return
		}
	}
	tz := Tz{Name: name, Shift: shift}
	self.Timezones = append(self.Timezones, tz)
}

func (self *Settings) RmTz(name string) bool {
	update := giter.Filter(
		func(tz Tz) bool { return tz.Name != name },
		self.Timezones...,
	)
	changed := len(self.Timezones) != len(update)
	self.Timezones = update
	return changed
}

func (self *Settings) NotifShiftsInSeconds() []int {
	var result []int
	for _, min := range self.NotifShiftsInMinutes {
		result = append(result, min*60)
	}
	return result
}
