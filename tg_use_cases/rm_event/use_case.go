package rm_event

import (
	"errors"
	"strconv"
	"stream-scheduler/proto"
	"stream-scheduler/types"
)

type use_case struct {
	events_repo proto.IEventsRepo
}

func New(events_repo proto.IEventsRepo) proto.ITgUseCase {
	return use_case{events_repo: events_repo}
}

func (use_case) Command() string {
	return "/delevent"
}

func (self use_case) Execute(_ int64, _ types.User, text string) (string, error) {
	if text == "" {
		return "", errors.New("invalid command")
	}
	id, err := strconv.Atoi(text)
	if err != nil {
		return "", errors.New("id not a number")
	}
	_, err = self.events_repo.GetById(id)
	if err != nil {
		return "", err
	}
	err = self.events_repo.Remove(id)
	return "ok", err
}
