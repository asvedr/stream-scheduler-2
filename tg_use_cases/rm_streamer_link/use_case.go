package rm_streamer_link

import (
	"errors"
	"strconv"
	"stream-scheduler/proto"
	"stream-scheduler/tg_use_cases/common"
	"stream-scheduler/types"
)

type use_case struct {
	repo proto.IStreamersRepo
}

func New(
	repo proto.IStreamersRepo,
) proto.ITgUseCase {
	return use_case{repo: repo}
}

func (use_case) Command() string {
	return "/dellink"
}

func (self use_case) Execute(_ int64, _ types.User, text string) (string, error) {
	tokens := common.SplitBySpace(text)
	if len(tokens) != 2 {
		return "", errors.New("invalid command")
	}
	id, err := strconv.Atoi(tokens[0])
	if err != nil {
		return "", errors.New("invalid id")
	}
	err = self.repo.RemoveLinks(id, []string{tokens[1]})
	return "ok", err
}
