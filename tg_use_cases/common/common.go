package common

import (
	"strings"

	"gitlab.com/asvedr/giter"
)

func SplitBySpace(text string) []string {
	return giter.FilterMap(
		func(s string) *string {
			s = strings.TrimSpace(s)
			if s == "" {
				return nil
			}
			return &s
		},
		strings.Split(text, " ")...,
	)
}

func SplitToLines(text string) []string {
	return giter.FilterMap(
		func(s string) *string {
			s = strings.TrimSpace(s)
			if s == "" {
				return nil
			}
			return &s
		},
		strings.Split(text, "\n")...,
	)
}
