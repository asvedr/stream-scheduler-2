package set_lang

import (
	"errors"
	"stream-scheduler/proto"
	"stream-scheduler/types"
)

type use_case struct {
	repo proto.ISettingsRepo
}

func New(repo proto.ISettingsRepo) proto.ITgUseCase {
	return use_case{repo: repo}
}

func (use_case) Command() string {
	return "/setlang"
}

func (self use_case) Execute(_ int64, _ types.User, text string) (string, error) {
	lang := types.Lang(text)
	if types.LangAll[lang] != 1 {
		return "", errors.New("unknown language")
	}
	stt, err := self.repo.Get()
	if err != nil {
		return "", err
	}
	stt.Lang = lang
	return "ok", self.repo.Set(stt)
}
