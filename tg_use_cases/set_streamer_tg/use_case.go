package set_streamer_tg

import (
	"errors"
	"strconv"
	"stream-scheduler/proto"
	"stream-scheduler/types"
	"strings"
)

type use_case struct {
	streamers_repo proto.IStreamersRepo
	users_repo     proto.IUsersRepo
}

func New(
	streamers_repo proto.IStreamersRepo,
	users_repo proto.IUsersRepo,
) proto.ITgUseCase {
	return use_case{
		streamers_repo: streamers_repo,
		users_repo:     users_repo,
	}
}

func (use_case) Command() string {
	return "/settg"
}

func (self use_case) Execute(_ int64, usr types.User, text string) (string, error) {
	split := strings.Split(text, " ")
	if len(split) != 2 {
		return "", errors.New("invalid command")
	}
	id, err := strconv.Atoi(split[0])
	if err != nil {
		return "", errors.New("id is not number")
	}
	_, err = self.streamers_repo.GetById(id)
	if err != nil {
		return "", err
	}
	tg_id, err := self.reveal_id(usr, split[1])
	if err != nil {
		return "", err
	}
	err = self.streamers_repo.UpdTg(id, tg_id)
	return "ok", err
}

func (self use_case) reveal_id(usr types.User, text string) (int64, error) {
	if text == "me" {
		return usr.TgId, nil
	}
	id, err := strconv.ParseInt(text, 10, 64)
	if err == nil {
		return id, nil
	}
	got, err := self.users_repo.GetByLogin(text)
	if err != nil {
		return 0, err
	}
	return got.TgId, nil
}
