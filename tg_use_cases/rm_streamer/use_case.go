package rm_streamer

import (
	"errors"
	"strconv"
	"stream-scheduler/proto"
	"stream-scheduler/types"
	"strings"
)

type use_case struct {
	repo proto.IStreamersRepo
}

func New(
	repo proto.IStreamersRepo,
) proto.ITgUseCase {
	return use_case{repo: repo}
}

func (use_case) Command() string {
	return "/delstr"
}

func (self use_case) Execute(_ int64, _ types.User, text string) (string, error) {
	if strings.TrimSpace(text) == "" {
		return "", errors.New("invalid command")
	}
	id, err := strconv.Atoi(text)
	if err != nil {
		return "", errors.New("invalid id")
	}
	err = self.repo.Remove(id)
	return "ok", err
}
