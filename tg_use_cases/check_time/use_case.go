package check_time

import (
	"fmt"
	"stream-scheduler/proto"
	"stream-scheduler/types"
	"strings"
	"time"

	"gitlab.com/asvedr/giter"
)

type use_case struct {
	repo proto.ISettingsRepo
	now  proto.INow
}

func New(
	repo proto.ISettingsRepo,
	now proto.INow,
) proto.ITgUseCase {
	return use_case{repo: repo, now: now}
}

func (use_case) Command() string {
	return "/checktime"
}

func (self use_case) Execute(chat int64, sender types.User, text string) (string, error) {
	settings, err := self.repo.Get()
	if err != nil {
		return "", err
	}
	now := self.now.Now()
	lines := giter.Map(
		func(tz types.Tz) string {
			shifted := now.Add(time.Hour * time.Duration(tz.Shift))
			return fmt.Sprintf(
				"%s %d:%d",
				tz.Name,
				shifted.Hour(),
				shifted.Minute(),
			)
		},
		settings.Timezones...,
	)
	return strings.Join(lines, "\n"), nil
}
