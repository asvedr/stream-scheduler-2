package help

import (
	"stream-scheduler/proto"
	"stream-scheduler/types"
	"strings"
)

const block_streamer = "streamer"
const block_event = "event"
const block_time = "time"
const block_settings = "settings"

var blocks = []string{block_streamer, block_event, block_time, block_settings}

var ru_help = map[string][]string{
	block_streamer: {
		"/getstr - посмотреть список или детали стримера\n",
		"-  /getstr - список всех стримеров\n",
		"-  /getstr id/name/tglogin - посмотреть детали стримера\n",
		"/addstr - добавить стримера \"/addstr имя имя2 имя3...\" ",
		"у стримера должно быть указано минимум одно имя. Команда ",
		"вернет идентификатор(id) стримера\n",
		"/renstr - переименовать стримера \"/renstr id имя имя2 имя3...\"\n",
		"/settg - привязать стримера к акку в телеге\n",
		"-  \"/settg id me\" - привязать стримера к своему аккаунту\n",
		"-  \"/settg id login\" - привязать стримера к аккаунту с логином login\n",
		"/addlink - добавить стримеру ссылку \"/addlink id link\"\n",
		"/dellink - удалить стримеру ссылку \"/dellink id link\"\n",
		"/delstr - удалить стримера \"/delstr id\"\n",
	},
	block_event: {
		"/getevent - посмотреть список или детали события\n",
		"- \"/getevent\" - список стримов\n",
		"- \"/getevent event_id\" - детали стрима\n",
		"/addevent - новый стрим \"/addevent название⏎",
		"сообщение(необязательно)⏎",
		"время стрима(см раздел 'время')\".\n",
		"Стримером назначается тот кто стрим создал\n",
		"/seteventstr - поменять стримера у стрима\n",
		"- /seteventstr event_id me - назначить себя стримером\n",
		"- /seteventstr event_id none - удалить стримера у события\n",
		"- /seteventstr event_id имя/логин-в-телеге - назначить стримера по имени/логину\n",
		"/delevent - удалить стрим \"/delevent event_id\"",
	},
	block_time: {
		"Поддерживаемые варианты формата времени при создании события:\n\n",
		"\"через 20 минут\" - в минутах относительно сейчас\n\n",
		"\"15:50\" - сегодня в 15:50 в часовом поясе по москве\n\n",
		"\"15:50 cy\" или \"cy 15:50\" сегодня в 15:50 в указаном часовом поясе\n\n",
		`"завтра 10:00", "10:00 сегодня" - с указанием относительной даты`,
		"\n\n",
		`"завтра 10:00 cy", "завтра cy 10:00", "msk 10:00 сегодня" - с указанием относительной даты и пояса`,
		"\n\n",
		`"30.12 10:00", "10:00 30.12" - с указанием дня и месяца dd.mm (пояс тоже можно указать)`,
		"\n\n",
		`"30.12.2023 10:00", "10:00 30-12-2023" - с указанием абсолютной даты dd.mm.yyyy (пояс тоже можно указать)`,
	},
	block_settings: {
		"/settz - добавить/поменять пояс \"/settz <name> <shift>\"\n",
		"shift может быть +N часов или -N часов\n",
		"/deltz - удалить часовой пояс \"/rmtz <name>\"\n",
		"/checktime - проверить время в разных поясах\n",
		"/setlang - установть язык \"/setlang ru/en\"\n",
	},
}

var en_help = map[string][]string{
	block_streamer: {
		"/getstr - get streamers list or streamer details\n",
		"-  /getstr - the list of all streamers\n",
		"-  /getstr id/name/tglogin - check details of one\n",
		"/addstr - add a streamer \"/addstr name1 name2 name3...\" ",
		"the streamer must have at least one name. Command ",
		"will return unique id of the streamer\n",
		"/renstr - rename the streamer \"/renstr id name name2 name3...\"\n",
		"/settg - bind the streamer to a telegram account\n",
		"-  \"/settg id me\" - bind the streamer to me\n",
		"-  \"/settg id login\" - bind the streamer to account via login\n",
		"/addlink - add link to streamer \"/addlink id link\"\n",
		"/dellink - remove link from streamer \"/dellink id link\"\n",
		"/delstr - remove streamer \"/delstr id\"\n",
	},
	block_event: {
		"/getevent - get list or details of events in chat\n",
		"- \"/getevent\" - list of events\n",
		"- \"/getevent event_id\" - details of event\n",
		"/event - get detailt of event in chat \"/event event_id\"\n",
		"/addevent - new stream \"/addevent name⏎",
		"message(optional)⏎",
		"time(check 'time' section)\".\nStreamer = event creator by default\n",
		"/seteventstr - set a streamer to a stream(event)\n",
		"- /seteventstr event_id me - set me as a streamer\n",
		"- /seteventstr event_id none - remove streamer from event\n",
		"- /seteventstr event_id name/tg-login - set a streamer via name or telegram login\n",
		"/delevent - remove stream \"/delevent event_id\"",
	},
	block_time: {
		"Available variants to set datetime on events:\n\n",
		"\"after 20 min\" - releative time in munites\n\n",
		"\"15:50\" - today at 15:50 in default timezone\n\n",
		"\"15:50 cy\" or \"cy 15:50\" today at 15:50 in custom timezone\n\n",
		`"tomorrow 10:00", "10:00 today" - with relative date`,
		"\n\n",
		`"tomorrow 10:00 cy", "today cy 10:00", "msk 10:00 today" - with relative time and timezone`,
		"\n\n",
		`"30.12 10:00", "10:00 30.12" - with date DD.MM or DD-MM (timzone is optional)`,
		"\n\n",
		`"30.12.2023 10:00", "10:00 30-12-2023" - with absolute date DD.MM.YYYY or DD-MM-YYYY (timezone is optional)`,
	},
	block_settings: {
		"/settz - add/change a timezone \"/settz <name> <shift>\"\n",
		"The shift can be +hours or -hours\n",
		"/deltz - remove the timezone \"/rmtz <name>\"\n",
		"/checktime - check time in different timezones\n",
		"/setlang - set language \"/setlang ru/en\"\n",
	},
}

var by_lang = map[types.Lang]map[string][]string{
	types.LangRu: ru_help,
	types.LangEn: en_help,
}

var total_help = map[types.Lang][]string{
	types.LangRu: {
		"Используй \"/help <section>\" ",
		"что бы посмотреть справку по разделу.",
		"Доступные разделы: ",
	},
	types.LangEn: {
		"Use \"/help <section>\" ",
		"to see the information about specific sections.",
		"Available sections: ",
	},
}

var total_help_descr = map[types.Lang]map[string]string{
	types.LangRu: {
		block_streamer: "как управлять стримерами?",
		block_event:    "как управлять событиями?",
		block_time:     "как задавать время в событии?",
		block_settings: "как менять настройки робота?",
	},
	types.LangEn: {
		block_streamer: "how to manage streamers?",
		block_event:    "how to manage events?",
		block_time:     "how to set time in events?",
		block_settings: "how to manage robot settings?",
	},
}

type use_case struct {
	repo proto.ISettingsRepo
}

func New(
	repo proto.ISettingsRepo,
) proto.ITgUseCase {
	return use_case{repo: repo}
}

func (use_case) Command() string {
	return "/help"
}

func (self use_case) Execute(_ int64, _ types.User, text string) (string, error) {
	settings, err := self.repo.Get()
	if err != nil {
		return "", err
	}
	help_map := by_lang[settings.Lang]
	block := help_map[strings.ToLower(text)]
	if len(block) == 0 {
		return self.total_help(settings.Lang), nil
	}
	return strings.Join(block, ""), nil
}

func (self use_case) total_help(lang types.Lang) string {
	help := strings.Join(total_help[lang], "")
	help += strings.Join(blocks, ", ")
	for key, val := range total_help_descr[lang] {
		help += "\n- " + key + ": " + val
	}
	return help
}
