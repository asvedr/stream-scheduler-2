package get_streamer

import (
	"fmt"
	"stream-scheduler/proto"
	"stream-scheduler/types"
	"strings"
)

type use_case struct {
	streamers_repo proto.IStreamersRepo
	picker         proto.IStreamerPicker
}

func New(
	streamers_repo proto.IStreamersRepo,
	picker proto.IStreamerPicker,
) proto.ITgUseCase {
	return use_case{
		streamers_repo: streamers_repo,
		picker:         picker,
	}
}

func (use_case) Command() string {
	return "/getstr"
}

func (self use_case) Execute(_ int64, _ types.User, text string) (string, error) {
	if text == "" {
		return self.get_list()
	} else {
		return self.get_details(text)
	}
}

func (self use_case) get_list() (string, error) {
	streamers, err := self.streamers_repo.GetAll()
	if err != nil {
		return "", err
	}
	result := fmt.Sprintf("found: %d\n", len(streamers))
	for _, str := range streamers {
		result += fmt.Sprintf("\n%d: %s", str.Id, str.Name)
	}
	return result, nil
}

func (self use_case) get_details(src string) (string, error) {
	streamer, err := self.picker.GetStreamer(src)
	if err != nil {
		return "", err
	}
	names := []string{streamer.Name}
	result := fmt.Sprintf(
		"id: %d\nNames: %s\n",
		streamer.Id,
		strings.Join(append(names, streamer.Tags...), ", "),
	)
	if streamer.TgLogin != nil {
		result += "telegram: " + *streamer.TgLogin + "\n"
	}
	for _, link := range streamer.Links {
		result += link + "\n"
	}
	return result, nil
}
