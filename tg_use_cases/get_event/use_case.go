package get_event

import (
	"errors"
	"fmt"
	"strconv"
	"stream-scheduler/proto"
	"stream-scheduler/ss_errs"
	"stream-scheduler/types"
	"strings"

	"gitlab.com/asvedr/giter"
)

type use_case struct {
	events_repo    proto.IEventsRepo
	streamers_repo proto.IStreamersRepo
	time_serde     proto.ITimeSerDe
}

func New(
	events_repo proto.IEventsRepo,
	streamers_repo proto.IStreamersRepo,
	time_serde proto.ITimeSerDe,
) proto.ITgUseCase {
	return &use_case{
		events_repo:    events_repo,
		streamers_repo: streamers_repo,
		time_serde:     time_serde,
	}
}

func (self *use_case) Command() string {
	return "/getevent"
}

func (self *use_case) Execute(chat int64, _ types.User, text string) (string, error) {
	if text == "" {
		return self.get_list(chat)
	} else {
		return self.get_details(chat, text)
	}
}

func (self use_case) get_list(chat int64) (string, error) {
	events, err := self.events_repo.GetChatEvents(chat, true)
	if err != nil {
		return "", err
	}
	lines, err := giter.MapErr(self.show_short, events...)
	hdr := fmt.Sprintf("found: %d", len(events))
	lines = append([]string{hdr}, lines...)
	return strings.Join(lines, "\n\n"), nil
}

func (self use_case) show_short(ev *types.Event) (string, error) {
	tm, err := self.time_serde.Show(ev.Time)
	msg := fmt.Sprintf(
		"id: %d\n%s\ntime: %s",
		ev.Id,
		ev.Name,
		tm,
	)
	return msg, err
}

func (self use_case) get_details(chat int64, src string) (string, error) {
	id, err := strconv.Atoi(src)
	if err != nil {
		return "", errors.New("id is not a number")
	}
	ev, err := self.events_repo.GetById(id)
	if err != nil {
		return "", err
	}
	if ev.ChatId != chat {
		return "", ss_errs.ErrEventNotFound{}
	}
	msg := strings.TrimSpace(ev.Name + "\n" + ev.Message)
	result := fmt.Sprintf("id: %d\n%s", ev.Id, msg)
	if ev.StreamerId != nil {
		result += "\nstreamer: "
		result += self.get_streamer(*ev.StreamerId)
	}
	when, err := self.time_serde.Show(ev.Time)
	if err != nil {
		return "", err
	}
	return result + "\n" + when, nil
}

func (self use_case) get_streamer(id int) string {
	str, err := self.streamers_repo.GetById(id)
	if err != nil {
		return "N/A"
	}
	return str.Name
}
