package add_streamer

import (
	"errors"
	"fmt"
	"stream-scheduler/proto"
	"stream-scheduler/tg_use_cases/common"
	"stream-scheduler/types"
)

type use_case struct {
	repo proto.IStreamersRepo
}

func New(
	repo proto.IStreamersRepo,
) proto.ITgUseCase {
	return use_case{repo: repo}
}

func (use_case) Command() string {
	return "/addstr"
}

func (self use_case) Execute(_ int64, usr types.User, text string) (string, error) {
	names := common.SplitBySpace(text)
	if len(names) == 0 {
		return "", errors.New("invalid command")
	}
	id, err := self.repo.Create(types.NewStreamer{
		Name: names[0],
		Tags: names[1:],
		TgId: &usr.TgId,
	})
	return fmt.Sprintf("id: %d", id), err
}
