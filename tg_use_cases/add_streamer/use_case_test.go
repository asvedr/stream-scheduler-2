package add_streamer_test

import (
	"stream-scheduler/components/sqlite/streamers_repo"
	"stream-scheduler/proto"
	"stream-scheduler/tg_use_cases/add_streamer"
	"stream-scheduler/types"
	"testing"

	"github.com/stretchr/testify/assert"
	ss "gitlab.com/asvedr/safe_sqlite"
)

type setup struct {
	repo     proto.IStreamersRepo
	use_case proto.ITgUseCase
}

func make_setup(t *testing.T) setup {
	db, err := ss.New(":memory:")
	assert.Nil(t, err)
	repo, err := streamers_repo.New(db)
	assert.Nil(t, err)
	return setup{
		repo:     repo,
		use_case: add_streamer.New(repo),
	}
}

func TestCreateStreamet(t *testing.T) {
	setup := make_setup(t)
	tg_id := int64(321)
	res, err := setup.use_case.Execute(123, types.User{TgId: tg_id}, "abc")
	assert.Nil(t, err)
	assert.Equal(t, "id: 1", res)
	str, err := setup.repo.GetById(1)
	assert.Nil(t, err)
	assert.Equal(t, &tg_id, str.TgId)
}
