package set_event_streamer

import (
	"errors"
	"strconv"
	"stream-scheduler/proto"
	"stream-scheduler/ss_errs"
	"stream-scheduler/tg_use_cases/common"
	"stream-scheduler/types"
)

var ErrNotFound = errors.New("streamer not found")
var ErrIsNotStreamer = errors.New("user is not streamer")

type use_case struct {
	events_repo    proto.IEventsRepo
	streamers_repo proto.IStreamersRepo
	picker         proto.IStreamerPicker
}

func New(
	events_repo proto.IEventsRepo,
	streamers_repo proto.IStreamersRepo,
	picker proto.IStreamerPicker,
) proto.ITgUseCase {
	return use_case{
		events_repo:    events_repo,
		picker:         picker,
		streamers_repo: streamers_repo,
	}
}

func (use_case) Command() string {
	return "/seteventstr"
}

func (self use_case) Execute(_ int64, user types.User, text string) (string, error) {
	tokens := common.SplitBySpace(text)
	if len(tokens) != 2 {
		return "", errors.New("invalid command")
	}
	ev_id, err := strconv.Atoi(tokens[0])
	if err != nil {
		return "", errors.New("id not a number")
	}
	var streamer_id *int
	streamer_raw := tokens[1]
	switch streamer_raw {
	case "me":
		streamer_id, err = self.get_default_streamer_id(user.TgId)
	case "none":
		streamer_id = nil
	default:
		streamer_id, err = self.reveal_streamer_id(streamer_raw)
	}
	if err != nil {
		return "", err
	}
	_, err = self.events_repo.GetById(ev_id)
	if err != nil {
		return "", err
	}
	err = self.events_repo.UpdateInfo(types.UpdEventInfo{
		Id:         ev_id,
		StreamerId: &streamer_id,
	})
	return "ok", err
}

func (self use_case) get_default_streamer_id(tg_id int64) (*int, error) {
	streamer, err := self.streamers_repo.FindByTg(&tg_id, nil)
	if errors.Is(err, ss_errs.ErrStreamerNotFound{}) {
		return nil, nil
	}
	if err != nil {
		return nil, err
	}
	return &streamer.Id, nil
}

func (self use_case) reveal_streamer_id(src string) (*int, error) {
	streamer, err := self.picker.GetStreamer(src)
	if err != nil {
		return nil, err
	}
	return &streamer.Id, nil
}
