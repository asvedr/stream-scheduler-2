package add_event_test

import (
	"stream-scheduler/components/sqlite/events_repo"
	"stream-scheduler/components/sqlite/settings_repo"
	"stream-scheduler/components/sqlite/streamers_repo"
	"stream-scheduler/components/time_serde"
	"stream-scheduler/proto"
	"stream-scheduler/tg_use_cases/add_event"
	"stream-scheduler/types"
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
	ss "gitlab.com/asvedr/safe_sqlite"
)

type mock_now struct{ time time.Time }

type setup struct {
	events_repo    proto.IEventsRepo
	streamers_repo proto.IStreamersRepo
	settings_repo  proto.ISettingsRepo
	now            proto.INow
	use_case       proto.ITgUseCase
}

func (self *mock_now) Now() time.Time {
	return self.time
}

func make_setup(t *testing.T) setup {
	db, err := ss.New(":memory:")
	assert.Nil(t, err)
	cnf := &types.Config{}
	ev, err := events_repo.New(cnf, db)
	assert.Nil(t, err)
	str, err := streamers_repo.New(db)
	assert.Nil(t, err)
	stt, err := settings_repo.New(db)
	assert.Nil(t, err)
	now := &mock_now{time: time.Date(2020, 1, 1, 10, 30, 0, 0, time.UTC)}
	ts := time_serde.New(now, stt)
	return setup{
		events_repo:    ev,
		streamers_repo: str,
		settings_repo:  stt,
		now:            now,
		use_case:       add_event.New(ev, str, stt, ts, now),
	}
}

func TestCreateEvDefaultStreamer(t *testing.T) {
	setup := make_setup(t)
	tg_id := int64(123)
	streamer_id, err := setup.streamers_repo.Create(types.NewStreamer{
		Name: "abc",
		TgId: &tg_id,
	})
	assert.Nil(t, err)
	resp, err := setup.use_case.Execute(
		321,
		types.User{TgId: tg_id, Login: "login"},
		"new event\nafter 5 min",
	)
	assert.Nil(t, err)
	assert.Equal(t, resp, "id: 1")
	ev, err := setup.events_repo.GetById(1)
	assert.Nil(t, err)
	assert.Equal(t, &streamer_id, ev.StreamerId)
}
