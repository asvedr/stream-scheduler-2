package add_event

import (
	"errors"
	"fmt"
	"stream-scheduler/proto"
	"stream-scheduler/ss_errs"
	"stream-scheduler/tg_use_cases/common"
	"stream-scheduler/types"
	"strings"
	"time"
)

type use_case struct {
	events_repo    proto.IEventsRepo
	streamers_repo proto.IStreamersRepo
	settings_repo  proto.ISettingsRepo
	time_serde     proto.ITimeSerDe
	now            proto.INow
}

type parsed struct {
	name string
	msg  string
	time time.Time
}

func New(
	events_repo proto.IEventsRepo,
	streamers_repo proto.IStreamersRepo,
	settings_repo proto.ISettingsRepo,
	time_serde proto.ITimeSerDe,
	now proto.INow,
) proto.ITgUseCase {
	return use_case{
		events_repo:    events_repo,
		streamers_repo: streamers_repo,
		settings_repo:  settings_repo,
		time_serde:     time_serde,
		now:            now,
	}
}

func (use_case) Command() string {
	return "/addevent"
}

func (self use_case) Execute(chat int64, user types.User, text string) (string, error) {
	settings, err := self.settings_repo.Get()
	parsed, err := self.parse_text(text)
	if err != nil {
		return "", err
	}
	if parsed.time.Compare(self.now.Now()) <= 0 {
		return "", errors.New("time can not be in past")
	}
	streamer_id, err := self.get_default_streamer_id(user.TgId)
	if err != nil {
		return "", err
	}
	ev_id, err := self.events_repo.Create(types.NewEvent{
		Name:       parsed.name,
		ChatId:     chat,
		StreamerId: streamer_id,
		Time:       parsed.time,
		Message:    parsed.msg,
	})
	if err != nil {
		return "", err
	}
	err = self.events_repo.AddNotifTimes(
		ev_id,
		settings.NotifShiftsInSeconds(),
	)
	return fmt.Sprintf("id: %d", ev_id), err
}

func (self use_case) get_default_streamer_id(tg_id int64) (*int, error) {
	streamer, err := self.streamers_repo.FindByTg(&tg_id, nil)
	if errors.Is(err, ss_errs.ErrStreamerNotFound{}) {
		return nil, nil
	}
	if err != nil {
		return nil, err
	}
	return &streamer.Id, nil
}

func (self use_case) parse_text(text string) (parsed, error) {
	lines := common.SplitToLines(text)
	if len(lines) < 2 {
		return parsed{}, errors.New("invalid command")
	}
	name := lines[0]
	lines = lines[1:]
	ev_time, err := self.time_serde.Parse(lines[len(lines)-1])
	if err != nil {
		return parsed{}, errors.New("invalid time: " + err.Error())
	}
	msg := strings.Join(lines[:len(lines)-1], "\n")
	result := parsed{
		name: name,
		msg:  msg,
		time: ev_time,
	}
	return result, nil
}
