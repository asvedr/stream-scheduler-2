package rename_streamer

import (
	"errors"
	"strconv"
	"stream-scheduler/proto"
	"stream-scheduler/tg_use_cases/common"
	"stream-scheduler/types"
)

type use_case struct {
	repo proto.IStreamersRepo
}

func New(repo proto.IStreamersRepo) proto.ITgUseCase {
	return use_case{repo: repo}
}

func (use_case) Command() string {
	return "/renstr"
}

func (self use_case) Execute(chat int64, sender types.User, text string) (string, error) {
	split := common.SplitBySpace(text)
	id, err := strconv.Atoi(split[0])
	if err != nil {
		return "", errors.New("id is not number")
	}
	names := split[1:]
	if len(names) == 0 {
		return "", errors.New("name not set")
	}
	_, err = self.repo.GetById(id)
	if err != nil {
		return "", err
	}
	err = self.repo.UpdName(id, names[0], names[1:])
	return "ok", err
}
