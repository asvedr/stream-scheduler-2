package set_tz

import (
	"errors"
	"strconv"
	"stream-scheduler/proto"
	"stream-scheduler/tg_use_cases/common"
	"stream-scheduler/types"
	"strings"
)

type use_case struct {
	repo proto.ISettingsRepo
}

func New(
	repo proto.ISettingsRepo,
) proto.ITgUseCase {
	return use_case{repo: repo}
}

func (use_case) Command() string {
	return "/settz"
}

func (self use_case) Execute(_ int64, _ types.User, text string) (string, error) {
	split := common.SplitBySpace(text)
	name := split[0]
	val := strings.TrimSpace(split[1])
	if len(val) == 0 {
		return "", errors.New("invalid command")
	}
	var mult int
	switch val[0] {
	case '+':
		mult = 1
	case '-':
		mult = -1
	default:
		return "", errors.New("expected + or -")
	}
	num, err := strconv.Atoi(val[1:])
	if err != nil {
		return "", errors.New("invalid num")
	}
	settings, err := self.repo.Get()
	if err != nil {
		return "", err
	}
	settings.SetTz(name, num*mult)
	err = self.repo.Set(settings)
	return "ok", err
}
