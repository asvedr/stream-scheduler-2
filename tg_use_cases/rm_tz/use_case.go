package rm_tz

import (
	"errors"
	"stream-scheduler/proto"
	"stream-scheduler/types"
	"strings"
)

type use_case struct {
	repo proto.ISettingsRepo
}

func New(
	repo proto.ISettingsRepo,
) proto.ITgUseCase {
	return use_case{repo: repo}
}

func (use_case) Command() string {
	return "/deltz"
}

func (self use_case) Execute(_ int64, _ types.User, text string) (string, error) {
	val := strings.TrimSpace(text)
	if len(val) == 0 {
		return "", errors.New("invalid command")
	}
	settings, err := self.repo.Get()
	if err != nil {
		return "", err
	}
	changed := settings.RmTz(val)
	if !changed {
		return "", errors.New("tz not found")
	}
	return "ok", self.repo.Set(settings)
}
