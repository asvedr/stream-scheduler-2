package cleaner

import (
	"log"
	"stream-scheduler/proto"
	"time"
)

type Process struct {
	events_repo proto.IEventsRepo
}

func New(
	events_repo proto.IEventsRepo,
) proto.IProcess {
	return Process{events_repo: events_repo}
}

func (Process) Name() string {
	return "cleaner"
}

func (self Process) Run() {
	for {
		err := self.events_repo.RemoveOld()
		if err != nil {
			log.Printf("cleaner error: %v", err)
		}
		time.Sleep(time.Second)
	}
}
