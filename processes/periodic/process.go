package periodic

import (
	"log"
	"stream-scheduler/proto"
	"time"
)

type process struct {
	periodic proto.IPeriodic
	sleep    time.Duration
	name     string
}

func New(
	name string,
	periodic proto.IPeriodic,
	sleep time.Duration,
) proto.IProcess {
	return &process{
		periodic: periodic, sleep: sleep, name: name,
	}
}

func (self *process) Name() string {
	return self.name
}

func (self *process) Run() {
	for {
		err := self.periodic.Execute()
		if err != nil {
			log.Printf("%s error: %v", self.name, err)
		}
		time.Sleep(self.sleep)
	}
}
