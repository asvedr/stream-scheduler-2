package ui

import (
	"errors"
	"log"
	"stream-scheduler/proto"
	"stream-scheduler/types"
	"strings"
	"time"

	"github.com/asvedr/gotgbot/v2"
	"github.com/asvedr/gotgbot/v2/ext"
	"github.com/asvedr/gotgbot/v2/ext/handlers"
	"github.com/asvedr/gotgbot/v2/ext/handlers/filters/message"
)

type ui struct {
	config     *types.Config
	bot        *gotgbot.Bot
	handlers   []proto.ITgUseCase
	users_repo proto.IUsersRepo
	bot_login  string
}

func New(
	config *types.Config,
	bot *gotgbot.Bot,
	users_repo proto.IUsersRepo,
	handlers ...proto.ITgUseCase,
) proto.IProcess {
	bot_login := strings.ToLower(config.RobotLogin)
	bot_login = strings.TrimPrefix(bot_login, "@")
	return &ui{
		config:     config,
		bot:        bot,
		users_repo: users_repo,
		handlers:   handlers,
		bot_login:  bot_login,
	}
}

func (self *ui) Name() string {
	return "ui"
}

func (self *ui) Run() {
	for {
		err := self.bot.Reconnect(nil)
		if err == nil {
			err = self.main_loop()
		}
		log.Printf("TgUi error: %v", err)
		time.Sleep(time.Second)
	}
}

func (self *ui) main_loop() error {
	bot := self.bot
	dispatcher := ext.NewDispatcher(&ext.DispatcherOpts{
		// If an error is returned by a handler, log it and continue going.
		Error: func(b *gotgbot.Bot, ctx *ext.Context, err error) ext.DispatcherAction {
			log.Println("an error occurred while handling update:", err.Error())
			return ext.DispatcherActionNoop
		},
		MaxRoutines: ext.DefaultMaxRoutines,
	})
	updater := ext.NewUpdater(dispatcher, nil)

	handler := func(bot *gotgbot.Bot, ctx *ext.Context) error {
		return self.react(ctx)
	}

	// Add echo handler to reply to all text messages.
	dispatcher.AddHandler(
		handlers.NewMessage(message.Text, handler),
	)
	err := updater.StartPolling(bot, &ext.PollingOpts{
		DropPendingUpdates: false,
		GetUpdatesOpts: &gotgbot.GetUpdatesOpts{
			Timeout: 9,
			RequestOpts: &gotgbot.RequestOpts{
				Timeout: time.Second * 10,
			},
		},
	})
	if err != nil {
		log.Printf("failed to start polling: %v", err.Error())
		return err
	}
	log.Printf("%s has been started...\n", bot.User.Username)

	// Idle, to keep updates coming in, and avoid bot stopping.
	updater.Idle()
	return errors.New("stop idling")
}

func (self *ui) react(ctx *ext.Context) error {
	chat := ctx.EffectiveChat.Id
	tg_user := ctx.EffectiveUser
	user, err := self.update_user(types.User{
		TgId:    tg_user.Id,
		Login:   strings.TrimSpace(tg_user.Username),
		Name:    strings.TrimSpace(tg_user.FirstName + tg_user.LastName),
		IsAdmin: tg_user.Id == self.config.AdminId,
	})
	if err != nil {
		return err
	}
	msg := ctx.EffectiveMessage.Text
	msg = strings.ToLower(strings.TrimSpace(msg))
	handler, msg := self.find_handler(self.handlers, msg)
	if handler == nil {
		log.Printf("cmd not found for: %v", user.TgId)
		return nil
	}
	resp, err := handler.Execute(chat, *user, msg)
	if err != nil {
		resp = "ERR: " + err.Error()
	}
	if resp == "" {
		return nil
	}
	_, err = ctx.EffectiveMessage.Reply(self.bot, resp, nil)
	return err
}

func (self *ui) update_user(user types.User) (*types.User, error) {
	return self.users_repo.CreateOrUpdate(user)
}

func (self *ui) find_handler(handlers []proto.ITgUseCase, msg string) (proto.ITgUseCase, string) {
	for _, handler := range handlers {
		if strings.HasPrefix(msg, handler.Command()) {
			without_prefix := strings.TrimPrefix(msg, handler.Command())
			if self.config.RobotLogin != "" {
				without_prefix = self.trim_login(without_prefix)
			}
			trimmed := strings.TrimSpace(without_prefix)
			return handler, trimmed
		}
	}
	return nil, ""
}

func (self *ui) trim_login(command string) string {
	return strings.TrimPrefix(command, "@"+self.bot_login)
}
