package main

import (
	"os"
	"stream-scheduler/app"

	giter "gitlab.com/asvedr/giter"
)

var Version = "N/A"

func is_help() bool {
	if len(os.Args) != 2 {
		return false
	}
	arg := os.Args[1]
	return arg == "-h" || arg == "--help" || arg == "-v"
}

func main() {
	if is_help() {
		println("Version: " + Version)
		return
	}
	giter.Gather(
		app.CleanerProc.Get().Run,
		app.SenderProc.Get().Run,
		app.UIProc.Get().Run,
	)
}
